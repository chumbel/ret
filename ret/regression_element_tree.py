import numpy as np
import pandas as pd
from river.base.regressor import MiniBatchRegressor
from river.utils import dict2numpy
import river.base.typing as river_types

from ret.data_structures import TreeData
from ret.tree_structures import TreeStructure
from ret.least_squares_regressor import evaluate_model
from ret.message_passing import Communicator
from ret.printing import create_dataframe


class RegressionElementTree(MiniBatchRegressor):
    """implements a regression element tree"""

    def __init__(self,
                 comm: Communicator,
                 num_dimensions: int,
                 data_window: int = 5000,
                 evaluation_window: int = 1000,
                 significance_level: float = 0.001,
                 max_depth: int = 10,
                 record_time: bool = False):
        """create new regression element tree

        :param comm: Communicator used to exchange data with worker processes
        :param num_dimensions: number of features / dimensions of the predictor space
        :param data_window: number of samples that should be stored
        :param evaluation_window: number of samples used to compare different tree structures
        :param significance_level: level of statistical significance that has to be met to perform a split
        :param max_depth: maximum number of layers in the tree
        :param record_time: whether timings of the worker processes should be recorded
        """
        assert comm.is_manager
        self.comm = comm
        self.evaluation = int(evaluation_window)
        self.significance_level = float(significance_level)
        self.max_depth = int(max_depth)
        self.tree = TreeStructure()
        self.data = TreeData(int(num_dimensions), int(data_window))
        self.record_time = bool(record_time)
        self.num_samples: int = 0
        self.samples_since_regrow: int = 0
        self.is_running: bool = True
        # add root to data
        self.data.add_empty_node(self.tree.root)
        # exchange parameters
        self.comm.exchange_parameters(significance_level, max_depth, data_window, record_time)
        # start building of alternative tree
        self.comm.grow_new_tree(*self.data.get_newest_data(0))

    def learn_one(self, x: dict, y: river_types.RegTarget):
        """fit to a set of features `x` and a real-valued target `y`

        :param x: dictionary of numeric features
        :param y: numeric target
        :return: self
        """
        assert self.is_running
        # incorporate previous results
        self._process_results()
        # increment sample count
        self.num_samples += 1
        # convert features to numpy array
        coordinates = dict2numpy(x)
        # find leaf that contains the point in feature space given by x
        leaf = self.tree.find_leaf(coordinates)
        # add data
        self.data.add_data(leaf, coordinates, y)
        # get complete data
        xs, ys = self.data.get_data(leaf)
        # wait until a spot in the task queue is available, then compute new model on a worker process
        while not self.comm.try_update_leaf(leaf, xs, ys, split_allowed=(leaf.depth < self.max_depth)):
            self._process_results()

        self.samples_since_regrow += 1
        # incorporate results and send new tasks
        self._process_results()
        return self

    def learn_many(self, X: pd.DataFrame, y: pd.Series):
        """fit to a set of data points `X` and real-valued targets `y`

        :param X: table of numeric features
        :param y: series of numeric target values
        :return: self
        """
        assert self.is_running
        self._process_results()
        # convert to numpy arrays
        coordinates = X.to_numpy(dtype=float)
        values = y.to_numpy(dtype=float)
        # create set to store leaves that currently can't be updated
        pending_leaves = set()
        # iterate leaves with data
        for leaf, positions in self.tree.iterate_leaves_with_data(coordinates):
            # number of data points added to current leaf
            num_samples = np.count_nonzero(positions)
            # add data
            self.data.add_data(leaf, coordinates[positions], values[positions])
            # get complete data
            xs, ys = self.data.get_data(leaf)
            # check if a spot in the task queue is available. if so, compute new model on a worker process
            if not self.comm.try_update_leaf(leaf, xs, ys, split_allowed=(leaf.depth < self.max_depth)):
                # otherwise, add leaf to the list of pending leaves
                pending_leaves.add((leaf, num_samples))
            else:
                self.samples_since_regrow += num_samples

        # incorporate results and send new tasks
        self._process_results()
        # go over all leaves that could not be updated previously
        while len(pending_leaves) > 0:
            leaf, num_samples = pending_leaves.pop()
            # check if the leaf still exists, if not, discard the update
            if self.data.contains(leaf):
                # get complete data
                xs, ys = self.data.get_data(leaf)
                # check if a spot in the task queue is available. if so, compute new model on a worker process
                if not self.comm.try_update_leaf(leaf, xs, ys, split_allowed=(leaf.depth < self.max_depth)):
                    # otherwise, add leaf again to the list of pending leaves
                    pending_leaves.add((leaf, num_samples))
                else:
                    self.samples_since_regrow += num_samples
            self._process_results()

        return self

    def predict_one(self, x: dict) -> river_types.RegTarget:
        """predict the output of features `x`

        :param x: dictionary of numeric features
        :return: the value of the prediction
        """
        # incorporate all previous results
        self._process_results()
        # compute prediction
        coordinates = dict2numpy(x)
        leaf = self.tree.find_leaf(coordinates)
        return evaluate_model(self.data.get_model(leaf), coordinates)

    def predict_many(self, X: pd.DataFrame) -> pd.Series:
        """predict the output for several feature sets `X`

        :param X: table of numeric features
        :return: a series containing the predicted values"""
        # do conversion to numpy and back
        return pd.Series(self._predict_array(X.to_numpy(dtype=float)))

    def complete_tasks(self):
        """wait until all pending tasks are completed"""
        while not self.comm.empty:
            self._process_results()

    def end_computation(self):
        """finish tasks and stop all workers

        :return: if `record_time` was set, the timings of worker processes
        """
        self.is_running = False
        while not self.comm.stop():
            self._process_results()
        if self.record_time:
            timings = self.comm.exchange_timing()
            return timings

    def to_dataframe(self) -> pd.DataFrame:
        """create data frame with tree structure and linear models

        :return: data frame containing a description of all nodes in this RET"""
        return create_dataframe(self.tree, self.data)

    def _process_results(self):
        """get all results from the communicator and apply them"""

        for result in self.comm.get_updates():
            leaf = result.leaf
            tree = self.tree
            data = self.data
            if data.contains(leaf):
                # set linear model
                data.set_model(leaf, result.model)
                if result.feature is not None and result.threshold is not None:
                    # split leaf
                    left, right = tree.split_leaf(leaf, result.feature, result.threshold)
                    # split data
                    data.split_data(leaf, left, right, result.feature, result.threshold)

        result = self.comm.get_new_tree()
        if result is not None:
            alt_tree, alt_data = result
            # compare alternatives, keep the one that is better
            if self._alternative_is_better(alt_tree, alt_data):
                new_X, new_y = self.data.get_newest_data(self.samples_since_regrow)
                self.samples_since_regrow = 0
                self.tree = alt_tree
                self.data = alt_data
                # add samples collected since starting to construct a new tree
                self.learn_many(pd.DataFrame(new_X), pd.Series(new_y))

            # start regrowing of the tree on worker process
            if self.is_running:
                xs, ys = self.data.get_newest_data(self.data.max_size)
                self.comm.grow_new_tree(xs, ys)

    def _alternative_is_better(self, alt_tree: TreeStructure, alt_data: TreeData) -> bool:
        """test if alternative tree gives better approximation for the last data points than the current tree

        :return: whether the sum of absolute errors is smaller for the alternative tree"""
        xs, ys = self.data.get_newest_data(self.evaluation)
        y_current = self._predict_array(xs)
        y_alt = self._predict_array(xs, alt_tree, alt_data)
        return np.sum(np.abs(ys - y_current)) > np.sum(np.abs(ys - y_alt))

    def _predict_array(self, x: np.ndarray, tree: TreeStructure = None, data: TreeData = None) -> np.ndarray:
        """predict the value for several feature sets `x`

        :param x: array of feature sets
        :param tree: a tree, if None, the tree stored in this RET is used
        :param data: TreeData containing the data of all nodes in `tree`
        :return: the predicted values
        """
        if tree is None or data is None:
            tree = self.tree
            data = self.data
        y = np.empty(x.shape[0])
        for leaf, positions in tree.iterate_leaves_with_data(x):
            model = data.get_model(leaf)
            y[positions] = evaluate_model(model, x[positions])
        return y

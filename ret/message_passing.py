from mpi4py import MPI
from collections import deque
import numpy as np
import pandas as pd
import pickle as pkl

from ret.logger import Logger, DummyLogger


class TAG:
    """tags for different jobs"""
    NO_JOB: int = 0
    MESSAGE_SIZE: int = 1
    UPDATE_LEAF: int = 2
    UPDATE_NO_SPLIT: int = 3
    GROW_TREE: int = 4


class LeafUpdate:
    """object that contains the result of an update task and its metadata"""
    def __init__(self,
                 leaf,
                 linear_model,
                 split_feature=None,
                 split_threshold=None):
        """
        :param leaf: the leaf that was updated
        :param linear_model: parametric description of the new model for `leaf`
        :param split_feature: index of the feature according to which the data of `leaf` should be partitioned,
        None if no split should be done
        :param split_threshold: threshold value for the partition of data of `leaf`,
        None if no split should be done
        """
        self.leaf = leaf
        self.model = linear_model
        self.feature = split_feature
        self.threshold = split_threshold


class _LeafUpdateTask:
    """object that contains data for a new task and its metadata"""
    def __init__(self, leaf, data, split_allowed: bool):
        """
        :param data: array of samples that has to be used to complete the task
        :param leaf: leaf to which `data` belongs
        :param split_allowed: whether the leaf can be split
        """
        self.data = data
        self.leaf = leaf
        self.split_allowed: bool = split_allowed


class _Response:
    """object that contains data of a response from a worker and its meta-data"""
    def __init__(self, request: MPI.Request = None, tag: TAG = TAG.NO_JOB, data=None, leaf=None):
        """
        :param request: MPI.Request object generated when posting a non-blocking receive for the result
        :param tag: identifier of the type of data that is expected
        :param data: buffer to contain the result
        :param leaf: node to be updated, should be None iff tag is 'GROW_TREE'
        """
        self.request: MPI.Request = request
        self.tag: TAG = tag
        self.data = data
        self.leaf = leaf


class Communicator:
    """A class that manages the communication between manager
     and worker processes for a regression element tree"""
    def __init__(self,
                 comm: MPI.Comm,
                 stationary_data: bool = False,
                 max_capacity: int = None,
                 logfile: str = None):
        """

        :param comm: MPI.Comm that contains manager and worker processes
        :param stationary_data: boolean flag to indicate whether the data is expected to be stationary,
        this would indicate that no forester process is needed to regrow the regression tree
        :param max_capacity: number of pending tasks that can be hold by the communicator
        :param logfile: name of the file th which log-messages should be written.
        if None, no log is created
        """
        self._comm: MPI.Comm = comm
        self.rank: int = comm.Get_rank()
        self.size: int = comm.Get_size()
        self._manager: int = self.size - 1
        if stationary_data:
            self.num_workers: int = self.size - 1
            self._forester: int = None
        else:
            self.num_workers: int = self.size - 2
            self._forester: int = self.size - 2
        if self.is_manager:
            self.stationary: bool = stationary_data
            self._jobs = np.full(self.num_workers, TAG.NO_JOB, dtype=object)
            self._tasks: deque[_LeafUpdateTask] = deque(maxlen=max_capacity)
            self._results: deque[LeafUpdate] = deque()
            self._log = Logger(logfile) if logfile is not None else DummyLogger()
            self._alternative_tree_response = _Response()

    @property
    def is_manager(self) -> bool:
        """:return: whether this process is the manager process"""
        # the last process in the communicator is the manager
        return self.rank == self._manager

    @property
    def is_forester(self) -> bool:
        """:return: whether this process is responsible for rebuilding the regression tree"""
        return self.rank == self._forester

    @property
    def capacity(self) -> int:
        """:return: maximal number of tasks that can be stored on this communicator"""
        return self._tasks.maxlen

    @property
    def full(self) -> bool:
        """check if number of pending tasks reached the defined capacity

        :return: whether the number of currently enqueued tasks is equal (or larger) than the expected capacity

        This is only available for the manager process"""
        assert self.is_manager
        return self.capacity is not None and len(self._tasks) >= self.capacity

    @property
    def empty(self) -> bool:
        """check if any tasks are pending

        :return: whether there are tasks waiting for completion

        This is only available for the manager process"""
        assert self.is_manager
        return (np.all(self._jobs == TAG.NO_JOB)
                and self._alternative_tree_response.tag == TAG.NO_JOB
                and len(self._tasks) == 0
                and len(self._results) == 0)

    def log(self, message: str):
        """write message to the log file

        :param message: string to be logged

        This is only available for the manager process"""
        assert self.is_manager
        self._log.log(message)

    def exchange_parameters(self, significance_level: float = 0.,
                            max_depth: int = 0,
                            max_size: int = 0,
                            get_timing: bool = False) -> tuple[float, int, int, bool]:
        """exchange all required parameters

        :param significance_level: level of statistical significance that has to be met to perform a split
        :param max_depth: maximum number of layers in the tree
        :param max_size: number of samples that should be stored for a new tree
        :param get_timing: whether timings of the worker processes should be recorded
        :return: significance level, maximum depth, maximum, size, whether to record timing

        The manager has to provide values for all parameters. Parameters given by workers are ignored.

        This is a collective operation that has to be performed on all processes in the communicator
        before any tasks are distributed"""
        buffer = np.array([significance_level, max_depth, max_size, get_timing], dtype=float)
        self._comm.Bcast(buffer, self._manager)
        # start logger
        if self.is_manager:
            self._log.open()
        return buffer[0], int(buffer[1]), int(buffer[2]), bool(buffer[3])

    def exchange_timing(self, total_time: float = 0., working_time: float = 0.) -> pd.DataFrame:
        """collect the timing data from workers

        :param total_time: time of the whole run as measured on this process
        :param working_time: time spent for performing tasks on this process
        :return: data frame containing measured times if calling process is manager, otherwise None

        This is a collective operation that has to be performed on all processes in the communicator
        after all tasks have been completed"""
        send_buf = np.array([self.rank, total_time, working_time], dtype=float)
        recv_buf = np.empty((self.size, 3)) if self.is_manager else None
        self._comm.Gather(send_buf, recv_buf, self._manager)
        if self.is_manager:
            timings = pd.DataFrame().from_records(recv_buf, columns=["rank", "total", "working"])
            return timings[timings["rank"] != self._manager]
        else:
            return None

    def try_update_leaf(self, leaf, x, y, split_allowed=True) -> bool:
        """try to pass the data of a leaf to some worker process for calculation of new model and split

        :param leaf: leaf that should be updated
        :param x: array of feature values stored on `leaf`
        :param y: array of corresponding target values
        :param split_allowed: whether `leaf` can be split further

        :return: whether the update can be sent

        This is only available for the manager process"""
        assert self.is_manager
        # check if a pending task contains an update to the same leaf, if so, merge tasks
        for task in self._tasks:
            if task.leaf is leaf:
                task.data = np.column_stack((x, y))
                self._update()
                return True
        self._update()
        # check if queue is already full, add new task if not
        if not self.full:
            self._tasks.append(_LeafUpdateTask(leaf, np.column_stack((x, y)), split_allowed))
            self.log(f"created task for updating leaf {id(leaf)}")
            self._update()
            return True
        else:
            return False

    def grow_new_tree(self, x, y):
        """pass data to forester process to generate an alternative tree

        :param x: array of feature values from which a new tree should be constructed
        :param y: array of corresponding target values

        This is only available for the manager process"""
        assert self.is_manager
        # if stationary, don't grow alternative trees
        if self.stationary:
            return
        assert self._alternative_tree_response.tag == TAG.NO_JOB
        data = np.column_stack((x, y))
        # send the size of the data to the receiving worker
        message_size = np.array(data.shape)
        self._comm.Isend(message_size, self._forester, TAG.MESSAGE_SIZE)
        # send data
        self._comm.Isend(data, self._forester, TAG.GROW_TREE)
        # expect size of the resulting tree
        size = np.empty(1, dtype=int)
        request = self._comm.Irecv(size, self._forester, TAG.MESSAGE_SIZE)
        self._alternative_tree_response = _Response(request, TAG.MESSAGE_SIZE, size)
        self.log(f"sent task to grow new tree to process {self._forester}")

    def stop(self) -> bool:
        """send a stopping message to all workers

        :return: whether a stopping signal was sent

        This is only available for the manager process, and only if all jobs are finished"""
        assert self.is_manager
        if not self.empty:
            return False
        for worker in range(self.num_workers):
            self._comm.Isend(np.full(2, TAG.NO_JOB, dtype=int), worker, TAG.MESSAGE_SIZE)
        if not self.stationary:
            self._comm.Isend(np.full(2, TAG.NO_JOB, dtype=int), self._forester, TAG.MESSAGE_SIZE)
        self._log.log("SENT STOP SIGNAL")
        self._log.close()
        return True

    def get_task(self) -> tuple[int, np.ndarray, np.ndarray]:
        """get a new task

        :return: task TAG, array of feature values, array of target values

        This is only available for worker processes"""
        assert not self.is_manager
        # get the size of message
        size = np.empty(2, dtype=int)
        self._comm.Recv(size, self._manager, TAG.MESSAGE_SIZE)
        if np.all(size == TAG.NO_JOB):
            # no further jobs to be done
            return TAG.NO_JOB, None, None
        # get message
        data = np.empty(size)
        status = MPI.Status()
        self._comm.Recv(data, self._manager, status=status)
        # return type of task and needed data
        return status.Get_tag(), data[:, :-1], data[:, -1]

    def send_update(self, model: np.ndarray, feature: int = None, threshold: float = None):
        """send the result of an update to the manager process

        :param model: parametric description of the new model
        :param feature: index of the feature according to which data should be partitioned
        :param threshold: threshold value for the partition of data

        This is only available for worker processes"""
        assert not self.is_manager and not self.is_forester
        if feature is None or threshold is None:
            feature = np.infty
            threshold = np.infty
        self._comm.Isend(np.concatenate((model, [float(feature), threshold])), self._manager, TAG.UPDATE_LEAF)

    def send_new_tree(self, tree, data):
        """send the result of a new construction of the regression tree

        :param tree: new tree structure
        :param data: data used to build `tree`

        This is only available for the forester process"""
        assert self.is_forester
        # send size of the object
        buffer = pkl.dumps([tree, data])
        size = np.array([len(buffer)], dtype=int)
        self._comm.Isend(size, self._manager, TAG.MESSAGE_SIZE)
        # send data structure
        self._comm.Isend([buffer, MPI.BYTE], self._manager, TAG.GROW_TREE)

    def get_updates(self):
        """returns results of all completed tasks as a generator

        :return: completed tasks

        This is only available for the manager process
        """
        assert self.is_manager
        self._update()
        while len(self._results) > 0:
            yield self._results.popleft()

    def get_new_tree(self):
        """returns an alternative regression tree if it is available, otherwise returns none

        :return: tree and associated data or None"""
        assert self.is_manager
        self._update()
        if self._alternative_tree_response.tag == TAG.GROW_TREE:
            # obtain result
            new_tree = pkl.loads(self._alternative_tree_response.data)
            self._alternative_tree_response = _Response()
            return new_tree

    def _send_all(self):
        """send all pending tasks to free workers"""
        assert self.is_manager
        free_workers = np.nonzero(self._jobs == TAG.NO_JOB)[0]
        # iterate over all free workers in the communicator
        for worker in free_workers:
            if len(self._tasks) == 0:
                # stop if no more tasks are available
                return
            task: _LeafUpdateTask = self._tasks.popleft()
            # send the size of the data to the receiving worker
            message_size = np.array(task.data.shape)
            self._comm.Isend(message_size, worker, TAG.MESSAGE_SIZE)
            # send data
            if task.split_allowed:
                self._comm.Isend(task.data, worker, TAG.UPDATE_LEAF)
            else:
                self._comm.Isend(task.data, worker, TAG.UPDATE_NO_SPLIT)
            # prepare receiving results
            self.log(f"sent task for updating leaf {id(task.leaf)} to process {worker}")
            # get linear model and split suggestion
            response_data = np.empty(task.data.shape[1] + 2)
            request = self._comm.Irecv(response_data, worker, TAG.UPDATE_LEAF)
            self._jobs[worker] = _Response(request, TAG.UPDATE_LEAF, response_data, task.leaf)

    def _receive_all(self):
        assert self.is_manager
        """add all completed tasks to the list of responses"""
        pending_workers = np.nonzero(self._jobs != TAG.NO_JOB)[0]
        for worker in pending_workers:
            response: _Response = self._jobs[worker]
            if response.request.Test():
                # updating model finished
                # check if a split is suggested
                if response.data[-1] == np.infty or response.data[-2] == np.infty:
                    self.log(f"received result for updating leaf {id(response.leaf)}"
                             f" from process {worker}: no split")
                    # add task with no split to list
                    self._results.append(LeafUpdate(leaf=response.leaf,
                                                    linear_model=response.data[:-2]))
                else:
                    self.log(f"received result for updating leaf {id(response.leaf)}"
                             f" from process {worker}: split")
                    # add finished task to list
                    self._results.append(LeafUpdate(leaf=response.leaf,
                                                    linear_model=response.data[:-2],
                                                    split_feature=int(response.data[-2]),
                                                    split_threshold=response.data[-1]))
                # set worker to unoccupied
                self._jobs[worker] = TAG.NO_JOB

        # check if growing of alternative tree is completed
        response = self._alternative_tree_response
        if response.tag == TAG.MESSAGE_SIZE and response.request.Test():
            size = response.data
            # generation of alternative tree finished
            self.log(f"received alternative tree structure")
            buffer = bytearray(size[0])
            # receive tree
            self._comm.Recv([buffer, MPI.BYTE], self._forester, TAG.GROW_TREE)
            # store the data
            self._alternative_tree_response = _Response(None, TAG.GROW_TREE, buffer)

    def _update(self):
        """collect all finished responses and distribute pending tasks"""
        assert self.is_manager
        self._receive_all()
        self._send_all()

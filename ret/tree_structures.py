import numpy as np
from ret.nodes import Node, Leaf, Branch


class TreeStructure:
    """manage the structure of the regression element tree"""
    def __init__(self):
        """create a tree containing a single leaf"""
        self.root: Node = Leaf(0)

    def find_leaf(self, x: np.ndarray) -> Leaf:
        """find the leaf that contains point x

        :param x: array of feature values
        :return: leaf containing `x`"""
        parent: Node = self.root
        child: Node = parent.find(x)
        while child is not parent and child is not None:
            parent = child
            child = child.find(x)
        return child

    def replace_node(self, old: Node, new: Node):
        """replace node `old` with node `new`

        :param old: node contained in this tree
        :param new: new node to replace `old`, not contained in this tree"""
        parent = old.parent
        new.parent = parent
        # replace root
        if old is self.root:
            self.root = new
        # insert subtree
        elif parent.left is old:
            parent.left = new
        elif parent.right is old:
            parent.right = new
        else:
            raise KeyError("not possible to replace a node who is not a chlid of his parent branch")

    def split_leaf(self, leaf: Leaf, split_feature: int, split_threshold: float) -> tuple[Leaf, Leaf]:
        """replace leaf by a branch with given split feature and threshold

        :param leaf: a leaf contained in this tree
        :param split_feature: index of the feature according to which the data of `leaf` should be partitioned
        :param split_threshold: threshold value for the partition
        """
        # create a new branch
        branch = Branch(leaf.depth, leaf.parent, split_feature, split_threshold)
        # create child leaves
        new_depth = leaf.depth + 1
        branch.left = Leaf(new_depth, parent=branch)
        branch.right = Leaf(new_depth, parent=branch)
        # insert new branch in the parent
        self.replace_node(leaf, branch)
        return branch.left, branch.right

    def iterate_leaves(self):
        """iterate over all leaves of this tree in order

        :return: leaves contained in this tree"""
        yield from self.root.iterate_leaves()

    def iterate(self):
        """iterate over all nodes of this tree in pre-order

        :return: nodes contained in this tree"""
        yield from self.root.iterate_nodes()

    def iterate_leaves_with_data(self, x: np.ndarray):
        """iterate over all leaves that contain the points given by rows of x in order

        :param x: array of feature values

        :return: pairs containing a leaf and a boolean array indicating positions of rows in x
        that are contained in this leaf"""
        yield from self.root.iterate_leaves_with_data(x, np.full(x.shape[0], True, dtype=bool))

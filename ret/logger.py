class Logger:
    def __init__(self, filename):
        """setup a file for logging"""
        self._filename = filename + ".txt"
        self._logfile = None

    def open(self):
        """open log file"""
        self._logfile = open(self._filename, "w")

    def log(self, message):
        """write message to log file"""
        self._logfile.write(message + "\n")

    def close(self):
        """close log file"""
        self._logfile.close()


class DummyLogger(Logger):
    """a dummy implementation that does nothing"""
    def __init__(self):
        super().__init__("")

    def open(self):
        pass

    def log(self, message):
        pass

    def close(self):
        pass

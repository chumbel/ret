from scipy.stats import shapiro, norm, chi2
import numpy as np

import opt_bindings as opt


class Splitter:
    def __init__(self, significance_level: float, max_entries: int):
        self.k_pre = 2.1435469250725863 / abs(norm.ppf(1 - significance_level))**0.2
        self.alpha = significance_level
        max_cells = int(min(np.sqrt(max_entries / 5), self.k_pre * max_entries**0.2))
        # percentiles of the chi^2 distribution for all possible number of cells
        self.chi2_percentiles = [chi2.ppf(1 - significance_level, df=k**2) for k in range(max_cells)]

    def _get_num_cells(self, num_entries: int):
        """
        compute the number of cells for each dimension of the contingency table

        this computation follows the description of Bagnato et al, 2012, The Autodependogram

        :param num_entries: number of samples
        :return:
            number of cells
        """
        if num_entries <= 5:
            return 1
        return int(min(np.sqrt(num_entries / 5), self.k_pre * num_entries**0.2))

    def suggest_split(self,
                      coordinates: np.ndarray,
                      errors: np.ndarray) -> tuple[int, float] | None:
        """ Find split feature and split threshold

        :param coordinates: array of data points, where each row represents one entry and each
            column a coordinate in the feature space
        :param errors: 1d array , where each entry represents the error of a model
            approximating a function at points given in "coordinates"
        :return: tuple (split feature index, split threshold), or None if no split should be done
        """
        # as convention, a row of the contingency table is considered a range of error values
        # whereas a column is associated with a range of coordinate values
        num_entries = errors.shape[0]
        num_cells = self._get_num_cells(num_entries)
        # if only one cell or all errors insignificant, the test can't result in a split
        if num_cells <= 1 or np.allclose(errors, 0.):
            return None
        # compute chi2 statistics for all features
        chi2_statistic = opt.chi2(errors, coordinates, num_cells)

        # test for normality of errors
        if np.allclose(errors, np.max(errors)):
            p_normality = 0  # if all errors are in a small range, they are not normally distributed
        elif num_entries < 3 or num_entries >= 5000:
            p_normality = 1  # for too few / many values, the test does not work
        else:
            _, p_normality = shapiro(errors)

        # test if any feature has a test statistic to reject the hypothesis
        # test if probability of normality is below the prescribed significance
        split_feature = np.argmax(chi2_statistic)
        if chi2_statistic[split_feature] > self.chi2_percentiles[num_cells - 1] or p_normality < self.alpha:
            # if one of the above is the case, return a split suggestion
            # i.e. the feature with the highest statistic, its mean/median value as split threshold
            return split_feature, np.mean(coordinates[:, split_feature])

        else:
            return None

from scipy.linalg import lstsq
import numpy as np


def compute_model(coordinates: np.ndarray, target_values: np.ndarray) -> np.ndarray:
    """ compute the linear regression hyperplane that is the least squares fit to the given data points

    :param coordinates: array of data points, where each row represents one entry and each
        column a coordinate in the feature space
    :param target_values: 1d array , where each entry represents the actual value
        at the corresponding point in "coordinates"

    :return: array of coefficients parametrizing the regression hyperplane
    """
    # create matrix for least squares regression
    lhs_matrix = np.hstack((np.ones((coordinates.shape[0], 1)), coordinates))
    # check for inf / NaN values
    valid_rows = np.all(np.isfinite(lhs_matrix), axis=1)
    if np.any(valid_rows == False):
        # this should not happen
        print(f"encountered inf or NaN values in function compute_model")
        lhs_matrix = lhs_matrix[valid_rows]
        target_values = target_values[valid_rows]
    # calculate regression, returns zeros if matrices are empty
    coefficients, _, _, _ = lstsq(lhs_matrix, target_values, overwrite_a=True, lapack_driver="gelsy")
    return coefficients


def evaluate_model(model: np.ndarray, coordinates: np.ndarray) -> np.ndarray:
    """compute values of the linear model at given positions

    :param model: array of coefficients parametrizing a hyperplane
    :param coordinates: array of data points, where each row represents one entry and each
        column a coordinate in the feature space
    :return: array of values of the linear model at positions given by "coordinates"
    """
    return model[0] + coordinates.dot(model[1:])

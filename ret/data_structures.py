import numpy as np
from ret.nodes import Node


class NodeData:
    """container for data points and the linear model belonging to a node"""
    # initial size for data arrays
    init_capacity: int = 8

    def __init__(self, ndim: int):
        """create empty container

        :param ndim: number of features / dimensions of the predictor space
        """
        self._data_points: np.ndarray = np.empty(shape=(NodeData.init_capacity, ndim + 1))
        self.model: np.ndarray = np.zeros(shape=ndim + 1)
        self._begin: int = 0
        self._end: int = 0

    @property
    def capacity(self) -> int:
        """get the current storage capacity

        :return: capacity"""
        return self._data_points.shape[0]

    @property
    def size(self) -> int:
        """return the current number of entries

        :return: number of stored samples"""
        return self._end - self._begin

    @property
    def ndim(self) -> int:
        """return the number of features

        :returns: number of features / dimensions of the predictor space"""
        return self._data_points.shape[1] - 1

    def add_data(self, x: np.ndarray, y: float | np.ndarray):
        """add new data point(s) to the node

        :param x: array of feature values
        :param y: target value(s)"""
        # check dimensions of input
        if x.ndim == 1 and isinstance(y, float):
            num_new = 1
            x = x.reshape(1, x.shape[0])
        elif x.ndim == 2 and x.shape[0] == y.shape[0]:
            num_new = x.shape[0]
        else:
            raise ValueError("x may be at most two-dimensional, dimensions of x and y must match")
        # if no space left at end
        size = self.size
        num_remaining = self.capacity - self._end
        if num_remaining < num_new:
            # try to shift data up
            if self._begin + num_remaining >= num_new:
                self._data_points[0:size, :] = self._data_points[self._begin:self._end, :]
                self._end = size
                self._begin = 0

            # increase size of underlying array
            else:
                new_capacity: int = 2 ** int(np.ceil(np.log2(self.size + num_new)))
                new_array = np.empty(shape=(new_capacity, self.ndim + 1))
                new_array[0:size, :] = self._data_points[self._begin:self._end, :]
                self._data_points = new_array
                self._end = size
                self._begin = 0

        new_end = self._end + num_new
        self._data_points[self._end:new_end, 0:-1] = x
        self._data_points[self._end:new_end, -1] = y
        self._end = new_end

    def get_data(self) -> tuple[np.ndarray, np.ndarray]:
        """get a reference to the feature and target values stored in this node

        :return: feature and target values of all stored samples"""
        return self._data_points[self._begin:self._end, 0:-1], self._data_points[self._begin:self._end, -1]

    def get_newest_data(self, num_entries: int = 1) -> tuple[np.ndarray, np.ndarray]:
        """get a reference to the num_entries last data points added to this node

        :param num_entries: number of samples to be obtained
        :return: feature and target values of the newest `num_entries` stored samples"""
        start = max(self._end - num_entries, self._begin)
        return self._data_points[start:self._end, 0:-1], self._data_points[start:self._end, -1]

    def remove_oldest_data(self, num_entries: int = 1):
        """remove the num_entries oldest data points in this node

        :param num_entries: number of samples to be removed

        if num_entries is larger than the size of the node, delete all data points
        """
        if num_entries < self.size:
            self._begin += num_entries
        else:
            self._begin = 0
            self._end = 0

        # resize the array if less than 3/8 (0.375)
        size = self.size
        if self.capacity > NodeData.init_capacity and size / self.capacity <= 0.375:
            new_capacity = NodeData.init_capacity if size < NodeData.init_capacity else 2 ** int(np.ceil(np.log2(size)))
            new_array = np.empty(shape=(int(new_capacity), self.ndim + 1))
            new_array[0:size, :] = self._data_points[self._begin:self._end]
            self._data_points = new_array
            self._begin = 0
            self._end = size


class TreeData:
    """manage all current data points belonging to a set of nodes"""
    def __init__(self, num_dimensions: int, max_size: int):
        """create an empty instance
        
        :param num_dimensions: number of features / dimensions of the predictor space
        :param max_size: maximal number of data points that can be stored
        """
        assert num_dimensions > 0
        assert max_size > 0
        self.ndim: int = num_dimensions
        self.max_size = max_size
        # history contains chronological ordering, where each data point went
        self._history = np.zeros(shape=max_size, dtype=int)
        # the position where new data points are inserted
        self._current = 0
        self._indices: dict[Node, int] = {}
        self._next_index = 1
        self._nodes: dict[int, NodeData] = {}

    def contains(self, node: Node) -> bool:
        """check if given node is present

        :param node: a node
        :return: whether `node` is contained in this instance of TreeData"""
        return self._indices.get(node) is not None

    def add_empty_node(self, node: Node) -> NodeData:
        """add an empty node"""
        data = NodeData(self.ndim)
        index = self._next_index
        self._next_index += 1
        self._indices[node] = index
        self._nodes[index] = data
        return data

    def add_data(self, node: Node, x: np.ndarray, y: float | np.ndarray):
        """add data points to given node

        :param node: node to which the data should be added
        :param x: array of feature values
        :param y: target value(s) """
        # if this node does not exist in the storage, create a new
        if not self.contains(node):
            self.add_empty_node(node)
        # obtain node data structure
        index = self._indices[node]
        data: NodeData = self._nodes[index]
        if isinstance(y, np.ndarray):
            # several data points should be added
            assert x.shape[0] == y.shape[0] and x.shape[1] == self.ndim
            # remove old data if it is overwritten
            num_entries = y.shape[0]
            if num_entries > self.max_size:
                # add only the last max_size elements
                x = x[-self.max_size:, :]
                y = y[-self.max_size:]
                num_entries = self.max_size
            positions = self._remove_oldest_data(num_entries)

            # insert new entries in history
            self._history[positions] = index
            # advance current position
            self._current = (self._current + num_entries) % self.max_size

        else:
            # assume only one data point
            assert x.ndim == 1 and x.shape[0] == self.ndim
            y = float(y)
            # remove old data if it is overwritten
            old_data: NodeData = self._nodes.get(self._history[self._current])
            if old_data is not None:
                old_data.remove_oldest_data()
            # insert new
            self._history[self._current] = index
            # advance current position
            self._current = (self._current + 1) % self.max_size

        # add data to node
        data.add_data(x, y)
        # test that number of entries is consistent
        assert data.size == np.count_nonzero(self._history == index)

    def get_data(self, node: Node) -> tuple[np.ndarray, np.ndarray]:
        """get a reference to the feature and target values stored for the given node

        :param node: a node contained in this TreeData
        :return: feature and target values of all stored samples for `node`"""
        index = self._indices[node]
        data: NodeData = self._nodes[index]
        # test that number of entries is consistent
        assert data.size == np.count_nonzero(self._history == index)
        return data.get_data()

    def get_newest_data(self, num_entries: int = 1) -> tuple[np.ndarray, np.ndarray]:
        """get the last num_entries data points

        :param num_entries: number of samples to be obtained
        :return: feature and target values of the newest `num_entries` stored samples"""
        if num_entries <= 0:
            # if no entries are requested, return none
            return np.empty((0, self.ndim)), np.empty(0)
        # get available data points in chronological order
        nonzero_positions = np.nonzero(self._history)[0]
        displacement = np.searchsorted(nonzero_positions, self._current)
        nonzero_elements = np.roll(self._history[nonzero_positions], -displacement)
        max_size = nonzero_elements.shape[0]
        if num_entries >= max_size:
            # make sure that num_entries is not larger than the total array
            num_entries = max_size
            # all elements should be returned
            newest_data = nonzero_elements
        else:
            # extract the num_elements newest datapoints, which are located at the end of the nonzero elements
            newest_data = nonzero_elements[-num_entries:]
        node_indices, positions, counts = np.unique(newest_data, return_inverse=True, return_counts=True)
        xs = np.empty(shape=(num_entries, self.ndim))
        ys = np.empty(num_entries)
        # iterate through nodes
        for i in range(len(node_indices)):
            # obtain count elements and insert them at corresponding entries from indices
            xs[positions == i, :], ys[positions == i] = self._nodes[node_indices[i]].get_newest_data(counts[i])
        return xs, ys

    def split_data(self, parent: Node, left: Node, right: Node, feature: int, threshold: float):
        """split the data of a node into two new sets according to the given threshold

        :param parent: a node contained in this TreeData
        :param left: left child of node `parent`, a node not contained in this TreeData
        :param right: right child of node `parent`, a node not contained in this TreeData
        :param feature: index of the feature according to which the data should be partitioned
        :param threshold: threshold value for the partition

        All samples with a value of feature `feature` strictly less than `threshold` are assigned to
        the left child. All others belong to the right child.
        """
        parent_index = self._indices[parent]
        parent_data = self._nodes[parent_index]
        xs, ys = parent_data.get_data()
        # decide where each data point goes
        less = xs[:, feature] < threshold
        # distribute data
        left_data = self.add_empty_node(left)
        right_data = self.add_empty_node(right)
        left_data.add_data(xs[less, :], ys[less])
        right_data.add_data(xs[~less, :], ys[~less])
        left_data.model = parent_data.model
        right_data.model = parent_data.model
        # remove parent
        self._indices.pop(parent)
        self._nodes.pop(parent_index)
        # change history
        left_index = self._indices[left]
        right_index = self._indices[right]
        new_history = np.full(xs.shape[0], fill_value=right_index, dtype=int)
        new_history[less] = left_index
        positions = (self._history == parent_index)
        displacement = np.count_nonzero(positions[0:self._current])
        self._history[positions] = np.roll(new_history, displacement)
        # test that number of entries is consistent
        assert right_data.size == np.count_nonzero(self._history == right_index)
        assert left_data.size == np.count_nonzero(self._history == left_index)

    def set_model(self, node, model: np.ndarray):
        """ set linear model of a given node to a new array

        :param node: a node contained in this TreeData
        :param model: array with parametric description of the new model for `node`
        """
        self._nodes[self._indices[node]].model = np.copy(model)

    def get_model(self, node) -> np.ndarray:
        """get the linear model of a given node

        :param node: a node contained in this TreeData
        :return:  array with parametric description of the model belonging to `node`"""
        return np.copy(self._nodes[self._indices[node]].model)

    def _remove_oldest_data(self, num_entries: int = 1) -> np.ndarray:
        """remove the oldest `num_entries` data points and get their positions in the history

        :param num_entries: number of samples to be removed
        :return: array of indices of the samples that have been removed from the history"""
        # assure that at most all elements are removed
        if num_entries > self.max_size:
            num_entries = self.max_size
        # check if all entries can be obtained going from current position forward (in terms of indices)
        abs_end = self._current + num_entries
        # get end of contiguous block of history starting in current position
        current_end = min(self.max_size, abs_end)
        current_start = self._current
        # get end of remaining rows at beginning, if some older values have to be accessed as well
        old_end = max(0, abs_end - self.max_size)
        old_start = 0
        # create index array for accessing history
        positions = np.concatenate((np.arange(current_start, current_end), np.arange(old_start, old_end)))
        # find how many entries are overwritten from different nodes
        node_indices, counts = np.unique(self._history[positions], return_counts=True)
        # remove entries in history
        self._history[positions] = 0
        # iterate through nodes and remove data points
        for index, count in zip(node_indices, counts):
            if index != 0:
                self._nodes[index].remove_oldest_data(count)
                # test that number of entries is consistent
                assert self._nodes[index].size == np.count_nonzero(self._history == index)

        return positions

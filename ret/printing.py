from collections import deque, defaultdict

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt, animation as animation
from mpl_toolkits.axes_grid1 import make_axes_locatable
from celluloid import Camera

from ret.data_structures import TreeData
from ret.nodes import Node, Branch, Leaf
from ret.least_squares_regressor import evaluate_model
from ret.tree_structures import TreeStructure


def create_dataframe(tree: TreeStructure, data: TreeData) -> pd.DataFrame:
    """create data frame containing the tree structure and models on leaves

    :param tree: a tree
    :param data: TreeData containing the data of all nodes in `tree`
    :return: data frame containing a description of all nodes in `tree`

    nodes of the tree are inserted in pre-order"""
    nodes = deque()
    # dictionary to allow continuous numbering of nodes
    # (if key is not found in dict, the length of the dict is returned)
    node_ids = defaultdict(lambda: len(node_ids))
    node_data = []
    nodes.append(tree.root)
    while len(nodes) > 0:
        node: Node = nodes.popleft()
        if isinstance(node, Branch):
            node_data.append({"node": node_ids[node],
                              "depth": node.depth,
                              "is_leaf": False,
                              "parent": node_ids[node.parent] if node.parent is not None else pd.NA,
                              "split_feature": node.feature,
                              "split_threshold": node.threshold,
                              "model": pd.NA})
            if node.left is not None:
                nodes.append(node.left)
            if node.right is not None:
                nodes.append(node.right)
        elif isinstance(node, Leaf):
            node_data.append({"node": node_ids[node],
                              "depth": node.depth,
                              "is_leaf": True,
                              "parent": node_ids[node.parent] if node.parent is not None else pd.NA,
                              "split_feature": pd.NA,
                              "split_threshold": pd.NA,
                              "model": data.get_model(node)})
    return pd.DataFrame.from_records(node_data).set_index("node")


def predict(table: pd.DataFrame, x: np.ndarray) -> np.ndarray:
    """predict the value for several feature sets  `x`

    :param table: containing a description of a RET
    :param x: array of feature sets

    :return: the predicted values
    """
    prediction = np.empty(x.shape[0])
    leaves = table[table["is_leaf"] == True]
    n_nodes = table.shape[0]
    n_dim = x.shape[1]
    cell_edges = np.empty((n_nodes, n_dim, 2))
    cell_edges[0, :, 0] = -np.infty
    cell_edges[0, :, 1] = np.infty
    for index, node in table.iterrows():
        if node["parent"] is pd.NA:
            continue
        parent_idx = int(node["parent"])
        feature = table["split_feature"].iloc[parent_idx]
        threshold = table["split_threshold"].iloc[parent_idx]
        # even nodes are right children, odd nodes left
        cell_edges[index] = cell_edges[parent_idx]
        cell_edges[index, feature, index % 2] = threshold
    for index, leaf in leaves.iterrows():
        positions = np.logical_and.reduce(
            np.logical_and(cell_edges[index, :, 0] <= x, x < cell_edges[index, :, 1]), axis=1)
        prediction[positions] = evaluate_model(leaf["model"], x[positions, :])
    return prediction


def get_corners_from_boundaries(boundaries):
    """obtain the corners of a subdomain given by its boundary values as min-max pairs

    :param boundaries: an array containing boundary values of a subdomain
    :return: coordinates of the corners of the subdomain
    """
    n_dim = boundaries.shape[0]
    if n_dim > 1:
        return np.stack(np.meshgrid(*[boundaries[i] for i in range(n_dim)]), -1).reshape(-1, n_dim)
    else:
        return boundaries[0].reshape(-1, n_dim)


def predict_edges(table: pd.DataFrame, x_min, x_max):
    """predict the value at corners of all subdomains associated with a leaf

    :param table: containing a description of a RET
    :param x_min: minimum value(s) for features
    :param x_max: maximum value(s) for features

    :return: predicted values at corners of subdomains
    """
    leaves = table[table["is_leaf"] == True]
    n_nodes = table.shape[0]
    n_dim = leaves["model"].iloc[-1].shape[0] - 1
    cell_edges = np.empty((n_nodes, n_dim, 2))
    cell_edges[0, :, 0] = x_min
    cell_edges[0, :, 1] = x_max
    for index, node in table.iterrows():
        if node["parent"] is pd.NA:
            continue
        threshold = table["split_threshold"].iloc[node["parent"]]
        feature = table["split_feature"].iloc[node["parent"]]
        # even nodes are right children, odd nodes left
        cell_edges[index] = cell_edges[node["parent"]]
        cell_edges[index, feature, index % 2] = threshold
    predictions = np.empty((n_nodes, 2**n_dim))
    for index, leaf in leaves.iterrows():
        corners = get_corners_from_boundaries(cell_edges[index])
        predictions[index] = evaluate_model(leaf["model"], corners)
    return predictions[leaves.index], cell_edges[leaves.index]


def predict_edges_in_section(table, x_min, x_max, section_feature, section_value):
    """predict the value at corners of all subdomains associated with a leaf that are bisected by the section
    on feature `section_feature` at value `section_value`

    :param table: containing a description of a RET
    :param x_min: minimum value(s) for features
    :param x_max: maximum value(s) for features
    :param section_feature: index of feature in which the domain should be bisected
    :param section_value: value at which the `section_feature` should be bisected

    :return: predicted values at corners of bisected subdomains
    """
    leaves = table[table["is_leaf"] == True]
    n_nodes = table.shape[0]
    n_dim = leaves["model"].iloc[-1].shape[0] - 1
    cell_edges = np.empty((n_nodes, n_dim, 2))
    cell_edges[0, :, 0] = x_min
    cell_edges[0, :, 1] = x_max
    for index, node in table.iterrows():
        if node["parent"] is pd.NA:
            continue
        threshold = table["split_threshold"].iloc[node["parent"]]
        feature = table["split_feature"].iloc[node["parent"]]
        # even nodes are right children, odd nodes left
        cell_edges[index] = cell_edges[node["parent"]]
        cell_edges[index, feature, index % 2] = threshold
    intersected_cell_edges = []
    predictions = []
    for index, leaf in leaves.iterrows():
        edges = cell_edges[index]
        if np.all(np.logical_and(edges[section_feature, 0] <= section_value,
                                 edges[section_feature, 1] > section_value)):
            intersected_cell_edges.append(edges)
            corners = get_corners_from_boundaries(edges)
            corners[:, section_feature] = section_value
            corners = np.unique(corners, axis=0)
            prediction = evaluate_model(leaf["model"], corners)
            predictions.append(prediction)

    return predictions, intersected_cell_edges


def plot_1d(table: pd.DataFrame, x: np.ndarray, y_true: np.ndarray,
            show: bool = True, file: str = None):
    """for a one-dimensional target, plot true function and approximation

    :param table: data frame representing a RET
    :param x: array of feature values
    :param y_true: the corresponding true target values
    :param show: whether the plot should be displayed
    :param file: the filename used to store the plot, if None, no plot is stored
    """
    assert x.shape[1] == 1
    plt.plot(x, y_true, "k--", label="exact")
    x_min = np.min(x)
    y_pred, cells = predict_edges(table, x_min, np.max(x))
    line = None
    for prediction, cell in zip(y_pred, cells[:, 0, :]):
        line, = plt.plot(cell, prediction, color="tab:blue")
        if cell[0] > x_min:
            plt.axvline(cell[0], color="grey", linewidth=0.5)
    if line is not None:
        line.set_label("approximation")  # noqa
    plt.legend(loc="lower right")
    plt.title("Piecewise linear Approximation using a Regression Element Tree")
    if file is not None:
        plt.savefig(file + ".pdf")
    if show:
        plt.show()


def plot_2d_contour(table: pd.DataFrame, x1: np.ndarray, x2: np.ndarray, y_true: np.ndarray,
                    show: bool = True, file: str = None):
    """ plot the regression surface and exact solution for a two-dimensional problem

    :param table: data frame representing a RET
    :param x1: values of first feature on a grid
    :param x2: values of second feature the same grid
    :param y_true: target values on the same grid
    :param show: whether the plot should be displayed
    :param file: the filename used to store the plot, if None, no plot is stored
    """
    assert x1.shape == x2.shape == y_true.shape
    fig, ax = plt.subplots()
    y_pred, cells = predict_edges(table, [np.min(x1), np.min(x2)], [np.max(x1), np.max(x2)])
    y_min = np.min(y_pred)
    y_max = np.max(y_pred)
    contour_exact = ax.contour(x1, x2, y_true, colors="black")
    img = None
    for prediction, cell in zip(y_pred, cells):
        img = ax.pcolormesh(cell[0], cell[1], prediction.reshape(2, 2), shading="gouraud", vmin=y_min, vmax=y_max)
        # draw borders of the cell
        ax.vlines(cell[0], ymin=[cell[1, 0]]*2, ymax=[cell[1, 1]]*2, color="grey", linewidth=0.5)
        ax.hlines(cell[1], xmin=[cell[0, 0]]*2, xmax=[cell[0, 1]]*2, color="grey", linewidth=0.5)
    # plt.contourf(x1, x2, y_pred.reshape(y_true.shape))
    ax.clabel(contour_exact, inline=True, fontsize=10)
    plt.title("Piecewise linear Approximation using a Regression Element Tree")
    plt.colorbar(mappable=img, label="value of approximation")
    if file is not None:
        plt.savefig(file + ".pdf")
    if show:
        plt.show()


def animate_1d(table_generator, x, y_func, t, show=True, file=None):
    """for a one-dimensional target, animate the evolution of true function and approximation

    :param table_generator: a generator for data frames representing a RET at different time points
    :param x: array of feature values
    :param y_func: function to compute the target value at different time points
    :param t: array of time points to be considered
    :param show: whether the plot should be displayed
    :param file: the filename used to store the plot, if None, no plot is stored
    """
    fig, ax = plt.subplots()
    cam = Camera(fig)
    x_min = np.min(x)
    x_max = np.max(x)
    line_approx = None
    line_exact = None
    for table, time in zip(table_generator, t):
        y_true = y_func(x, time)
        line_exact, = plt.plot(x, y_true, "k--")
        y_pred, cells = predict_edges(table, x_min, x_max)
        for prediction, cell in zip(y_pred, cells[:, 0, :]):
            line_approx, = plt.plot(cell, prediction, color="tab:blue")
            if cell[0] > x_min:
                plt.axvline(cell[0], color="grey", linewidth=0.5)
        ax.text(0.8, 0.9, f"n = {time}", transform=ax.transAxes)
        cam.snap()

    if line_approx is not None:
        line_approx.set_label("approximation")
    if line_exact is not None:
        line_exact.set_label("exact")
    plt.title("Piecewise linear Approximation using a Regression Element Tree")
    plt.legend(loc="lower right")
    animation = cam.animate()
    if show:
        plt.show()
    if file is not None:
        animation.save(file + ".mp4", fps=5)


def animate_2d(table_generator, x1, x2, y_func, t, n_frames, points=None, show=True, file=None):
    """for a two-dimensional target, animate the evolution of true function and approximation

    :param table_generator: a generator for data frames representing a RET at different time points
    :param x1: values of first feature on a grid
    :param x2: values of second feature the same grid
    :param y_func: function to compute the target value at different time points
    :param t: array of time points to be considered
    :param n_frames: number of frames to be generated for the animation,
        generators must provide at least `n_frames`elements
    :param points: generator of arrays containing coordinates of samples that were used in RET at different time points
    :param show: whether the plot should be displayed
    :param file: the filename used to store the plot, if None, no plot is stored
    """
    fig, ax = plt.subplots()
    div = make_axes_locatable(ax)
    cax = div.append_axes('right', '5%', '5%')

    x_min = np.array(np.min(x1), np.min(x2))
    x_max = np.array(np.max(x1), np.max(x2))

    def animate(args):
        ax.cla()
        if points is not None:
            table, time, point_coordinates = args
        else:
            table, time = args
            point_coordinates = None
        y_true = y_func(x1, x2, time)
        y_pred, cells = predict_edges(table, x_min, x_max)
        y_min = np.min(y_pred)
        y_max = np.max(y_pred)
        contour_exact = ax.contour(x1, x2, y_true, colors="black")
        colour_approx = None
        for prediction, cell in zip(y_pred, cells):
            colour_approx = ax.pcolormesh(cell[0], cell[1], prediction.reshape(2, 2),
                                          shading="gouraud", vmin=y_min, vmax=y_max)
            # draw borders of the cell
            ax.vlines(cell[0], ymin=[cell[1, 0]] * 2, ymax=[cell[1, 1]] * 2, color="grey", linewidth=0.5)
            ax.hlines(cell[1], xmin=[cell[0, 0]] * 2, xmax=[cell[0, 1]] * 2, color="grey", linewidth=0.5)
        if points is not None:
            ax.scatter(point_coordinates[:, 0], point_coordinates[:, 1], marker=".", color="red")
        ax.clabel(contour_exact, inline=True, fontsize=10)
        ax.text(0.8, 0.9, f"n = {time}", transform=ax.transAxes)
        cax.cla()
        fig.colorbar(colour_approx, cax=cax)

    if points is not None:
        anim = animation.FuncAnimation(fig, animate, frames=zip(table_generator, t, points), save_count=n_frames,
                                       repeat=False, interval=500)
    else:
        anim = animation.FuncAnimation(fig, animate, frames=zip(table_generator, t), save_count=n_frames,
                                       repeat=False, interval=500)
    if show:
        plt.show()
    if file is not None:
        anim.save(file + ".mp4", fps=3)

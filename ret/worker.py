from collections import deque
from time import perf_counter

import numpy as np

from ret.data_structures import TreeData
from ret.least_squares_regressor import compute_model, evaluate_model
from ret.nodes import Leaf
from ret.splitter import Splitter
from ret.tree_structures import TreeStructure
from ret.message_passing import Communicator, TAG


def update_model(leaf: Leaf, data: TreeData):
    """recompute the linear model on the given leaf

    :param leaf: a leaf
    :param data: a TreeData containing the data of `leaf`"""
    # obtain data of leaf
    x, y = data.get_data(leaf)
    # compute model and errors
    model = compute_model(x, y)
    data.set_model(leaf, model)


class Worker:
    def __init__(self, comm: Communicator = None, significance_level=None, max_depth=None, max_size=None):
        """Create a worker to perform different tasks on the regression element tree

        :param comm: a communicator used to pass tasks and results between manager and worker processes
        :param significance_level: required significance level for statistical tests
        :param max_depth: maximal depth of the tree
        :param max_size: maximal number of data points that can be stored

        Users should provide either
        a communicator and no other parameters
        or no communicator but all other parameters.
        """
        self.comm = comm
        if self.comm is not None:
            assert not comm.is_manager
            # get the required significance level for statistical tests
            self.significance_level, self.max_depth, self.max_size, self.record_time = comm.exchange_parameters()
        else:
            self.significance_level = significance_level
            self.max_depth = max_depth
            self.max_size = max_size
            self.record_time = False
        # create a splitter
        self.splitter = Splitter(self.significance_level, self.max_size)

    def try_split(self, leaf: Leaf, tree: TreeStructure, data: TreeData) -> tuple[Leaf, Leaf] | None:
        """attempt to split the given leaf and return the resulting pair of leaves

        :param leaf: a leaf
        :param tree: a tree containing `leaf`
        :param data: a TreeData containing data of all nodes in `tree`
        :return: left and right child resulting from the split, None if no split is performed"""
        # obtain data of leaf
        x, y = data.get_data(leaf)
        # compute model and errors
        model = compute_model(x, y)
        y_eval = evaluate_model(model, x)
        errors = np.abs(y - y_eval)
        # store model in leaf data
        data.set_model(leaf, model)
        # get split suggestion
        split = self.splitter.suggest_split(x, errors)
        if split is None:
            # no split was suggested
            return None
        # split leaf in tree
        left, right = tree.split_leaf(leaf, split_feature=split[0], split_threshold=split[1])
        # split data
        data.split_data(leaf, left, right, feature=split[0], threshold=split[1])
        return left, right

    def build_tree(self, coordinates: np.ndarray, target_values: np.ndarray) -> tuple[TreeStructure, TreeData]:
        """create a regression element tree form given data

        :param coordinates: array of feature values
        :param target_values: array of corresponding target values
        :return: the new tree and corresponding data
        """
        # create empty tree and data structure
        tree = TreeStructure()
        data = TreeData(num_dimensions=coordinates.shape[1], max_size=self.max_size)
        data.add_data(tree.root, coordinates, target_values)
        # create set of leaves for which a split could be attempted
        leaves_to_split = deque([tree.root])
        while len(leaves_to_split) > 0:
            # get next leaf from set
            leaf = leaves_to_split.popleft()
            # try to split leaf if max_depth is not reached
            if leaf.depth < self.max_depth:
                children = self.try_split(leaf, tree, data)
                # if split succeeded, add new leaves to the list
                if children is not None:
                    leaves_to_split.extend(children)
            else:
                # otherwise, recompute model on the leaf
                update_model(leaf, data)
        return tree, data

    def run(self):
        """perform tasks provided via the communicator

        this is only available if the worker was created with a communicator"""
        assert self.comm is not None and not self.comm.is_manager
        total_time = 0
        working_time = 0
        # wait to get tasks
        if self.record_time:
            total_time = -perf_counter()
        while True:
            task = self.comm.get_task()
            if task[0] == TAG.NO_JOB:
                # stop if termination signal received
                if self.record_time:
                    total_time += perf_counter()
                    self.comm.exchange_timing(total_time, working_time)
                return

            if self.record_time:
                working_time -= perf_counter()

            if task[0] == TAG.UPDATE_LEAF or task[0] == TAG.UPDATE_NO_SPLIT:
                # update model
                x = task[1]
                y = task[2]
                model = compute_model(x, y)
                y_eval = evaluate_model(model, x)
                errors = np.abs(y - y_eval)
                split = None
                if task[0] == TAG.UPDATE_LEAF:
                    # get split suggestion
                    split = self.splitter.suggest_split(x, errors)
                # send result
                if split is None:
                    self.comm.send_update(model)
                else:
                    self.comm.send_update(model, split[0], split[1])

            elif task[0] == TAG.GROW_TREE:
                # build new tree
                tree, data = self.build_tree(task[1], task[2])
                # send result
                self.comm.send_new_tree(tree, data)

            if self.record_time:
                working_time += perf_counter()
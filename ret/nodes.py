import abc
import numpy as np


class Node(abc.ABC):
    """generic node in the ree representation of a RET"""

    def __init__(self, depth: int = 0, parent=None):
        self.depth = depth
        self.parent = parent

    @abc.abstractmethod
    def find(self, x: np.ndarray):
        """return the child node containing point x

        :param x: array of feature values
        :return: node containing `x`"""

    @abc.abstractmethod
    def iterate_leaves(self):
        """iterate over all leaves in order"""

    @abc.abstractmethod
    def iterate_nodes(self):
        """iterate over all nodes in pre-order"""

    @abc.abstractmethod
    def iterate_leaves_with_data(self, x: np.ndarray, positions: np.ndarray):
        """iterate over all leaves that contain the points given by rows of x in order

        :param x: array of feature values
        :param positions: boolean array indicating which rows of `x` should be considered

        :return: pairs containing a leaf and a boolean array indicating positions of rows in x
        that are contained in this leaf"""


class Branch(Node):
    """representation of a splitting point in the tree representation of a RET"""

    def __init__(self,
                 depth: int = 0,
                 parent: Node = None,
                 split_feature: int = None,
                 split_threshold: float = None,
                 left_child: Node = None,
                 right_child: Node = None):
        """
        :param depth: number of layers that are traversed when going from this node to the root
        :param parent: node of which this Branch is a child
        :param split_feature: index of the feature according to which samples are passed to the left or right child
        :param split_threshold: threshold value according to which samples are passed to the left or right child
        :param left_child: left child of this node
        :param right_child: right child of this node

        Samples with a value of feature `split_feature` strictly less than `split_threshold` are passed to
        the left child. All others belong to the right child.
        """
        super().__init__(depth, parent)
        self.feature = split_feature
        self.threshold = split_threshold
        self.left = left_child
        self.right = right_child

    def find(self, x: np.ndarray) -> Node:
        if x[self.feature] < self.threshold:
            return self.left
        return self.right

    def iterate_leaves(self):
        if self.left is not None:
            yield from self.left.iterate_leaves()
        if self.right is not None:
            yield from self.right.iterate_leaves()

    def iterate_nodes(self):
        yield self
        if self.left is not None:
            yield from self.left.iterate_nodes()
        if self.right is not None:
            yield from self.right.iterate_nodes()

    def iterate_leaves_with_data(self, x: np.ndarray, positions: np.ndarray):
        less = np.logical_and(positions, x[:, self.feature] < self.threshold)
        if self.left is not None and np.any(less):
            yield from self.left.iterate_leaves_with_data(x, less)
        if self.right is not None and np.any(~less):
            yield from self.right.iterate_leaves_with_data(x, ~less)


class Leaf(Node):
    """representation of a splitting point in the tree representation of a RET"""

    def __init__(self, depth: int = 0, parent=None):
        """
        :param depth: number of layers that are traversed when going from this node to the root
        :param parent: node of which this Leaf is a child
        """
        super().__init__(depth, parent)

    def find(self, x: np.ndarray) -> Node:
        return self

    def iterate_leaves(self):
        yield self

    def iterate_nodes(self):
        yield self

    def iterate_leaves_with_data(self, x: np.ndarray, positions: np.ndarray):
        yield self, positions

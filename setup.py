from setuptools import setup
from pybind11.setup_helpers import Pybind11Extension, build_ext

ext_modules = [
    Pybind11Extension("opt_bindings",
                      include_dirs=['./cpp_src'],
                      sources=sorted(['cpp_src/opt_bindings.cpp']),
                      cxx_std=17,
                      extra_compile_args=['-funroll-loops'],
                      ),
]

setup(
    name='ret',
    version='2023.2',
    packages=['ret'],
    author='Ciril Humbel',
    author_email='chumbel@ethz.ch',
    description='online tree-based regression algorithm',
    ext_modules=ext_modules,
    cmdclass={"build_ext": build_ext},
)

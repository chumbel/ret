import unittest
import numpy as np

from ret.tree_structures import TreeStructure


class TreeTestCase(unittest.TestCase):
    def test_split(self):
        tree = TreeStructure()
        leaf = tree.find_leaf(np.random.random(1))
        tree.split_leaf(leaf, 0, 0.5)
        self.assertIsNot(tree.find_leaf([0.4]), tree.find_leaf([0.6]))

    def test_find(self):
        tree = TreeStructure()
        tree.split_leaf(tree.root, 0, 0.5)
        left, right = tree.split_leaf(tree.root.left, 0, 0.1)
        self.assertIs(left, tree.find_leaf([0.05]))
        self.assertIs(right, tree.find_leaf([0.25]))
        self.assertIs(tree.root.right, tree.find_leaf([0.8]))


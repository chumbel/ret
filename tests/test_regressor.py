import unittest
import numpy as np
from ret.least_squares_regressor import compute_model, evaluate_model


class LeastSquaresTestCase(unittest.TestCase):
    def test_constant(self):
        x = np.array([[1, 1], [2, 1], [1, 2]])
        y = np.array([1, 1, 1])
        model = compute_model(x, y)
        x_test = np.array([[0.5, 0.5], [3, 3], [2, 1]])
        # test prediction
        self.assertTrue(np.allclose(1, evaluate_model(model, x_test)))

    def test_linear(self):
        x = np.array([[0.2], [0.5], [0.8]])
        y = 4 * x + 2
        model = compute_model(x, y)
        x_test = x + 0.2
        y_test = 4 * x_test + 2
        self.assertTrue(np.allclose(evaluate_model(model, x_test), y_test))

import unittest
import numpy as np
from ret.worker import Worker
from ret.nodes import Leaf


class RecursiveTreeTestCase(unittest.TestCase):
    def test_no_split_constant(self):
        x = np.random.random((50, 2))
        y = np.ones(50)
        worker = Worker(significance_level=0.01, max_depth=10, max_size=x.shape[0])
        tree, data = worker.build_tree(x, y)
        self.assertTrue(isinstance(tree.root, Leaf))
        stored_x, stored_y = data.get_data(tree.root)
        self.assertTrue(np.all(x == stored_x))
        self.assertTrue(np.all(y == stored_y))

    def test_split_step(self):
        x = np.random.random((50, 2))
        x[::2, 0] += 1
        # y = 0 if x < 1, else 1
        y = np.where(x[:, 0] < 1, 0, 1)
        worker = Worker(significance_level=0.01, max_depth=10, max_size=x.shape[0])
        tree, data = worker.build_tree(x, y)
        self.assertFalse(isinstance(tree.root, Leaf))

    def test_trump(self):
        with np.load("trump_drift.npz", "r") as data:
            x = data["x"]
            y = data["y"]
        worker = Worker(significance_level=0.001, max_depth=10, max_size=x.shape[0])
        tree, data = worker.build_tree(x, y)
        self.assertTrue(np.all(x == data.get_newest_data(len(x))[0]))
        self.assertTrue(np.all(y == data.get_newest_data(len(y))[1]))

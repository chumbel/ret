from mpi4py import MPI
from random import random
import math

from river.datasets import TrumpApproval
from river.metrics import RMSE
from river.evaluate import progressive_val_score

from ret.regression_element_tree import RegressionElementTree
from ret.worker import Worker
from ret.message_passing import Communicator


def test_one(communicator):
    if communicator.is_manager:
        x = {"a": 1, "b": 1}
        y = 1
        regressor = RegressionElementTree(communicator, len(x))
        initial_prediction = regressor.predict_one(x)
        regressor.learn_one(x, y)
        regressor.end_computation()
        informed_prediction = regressor.predict_one(x)
        if math.isclose(initial_prediction, 0) and math.isclose(informed_prediction, y):
            print("test_one: SUCCESS")
        else:
            print("test_one: FAILED")
    else:
        worker = Worker(communicator)
        worker.run()


def test_constant_with_drift_detection(communicator):
    if communicator.is_manager:
        num_samples = 50
        xs = [{"a": random(), "b": random()} for _ in range(num_samples)]
        y = 1
        regressor = RegressionElementTree(communicator, len(xs[0]), evaluation_window=10)
        for x in xs:
            regressor.learn_one(x, y)
        regressor.end_computation()
        for x in xs:
            if not math.isclose(regressor.predict_one(x), y):
                print("test_constant_with_drift_detection: FAILED")
                return
        print("test_constant_with_drift_detection: SUCCESS")
    else:
        worker = Worker(communicator)
        worker.run()


def test_trump(communicator):
    if communicator.is_manager:
        dataset = TrumpApproval()
        model = RegressionElementTree(communicator, num_dimensions=dataset.n_features, evaluation_window=100,
                                      significance_level=0.01)
        error = RMSE()
        progressive_val_score(dataset, model, error)
        print(f"test_trump: finished with {error}")
        model.end_computation()
    else:
        worker = Worker(communicator)
        worker.run()


def test_trump_sequential(communicator):
    if communicator.is_manager:
        dataset = TrumpApproval()
        model = RegressionElementTree(communicator, num_dimensions=dataset.n_features, data_window=10000,
                                      evaluation_window=100, significance_level=0.001)
        error = RMSE()
        for x, y in dataset:
            y_pred = model.predict_one(x)
            error.update(y, y_pred)
            model.learn_one(x, y)
            model.complete_tasks()
        print(f"test_trump_sequential: finished with {error}")
        model.end_computation()
    else:
        worker = Worker(communicator)
        worker.run()


if __name__ == "__main__":
    comm = Communicator(MPI.COMM_WORLD, stationary_data=True)
    test_one(comm)
    test_constant_with_drift_detection(comm)
    test_trump(comm)
    test_trump_sequential(comm)

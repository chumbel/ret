import unittest
import numpy as np

from ret.nodes import Leaf
from ret.data_structures import NodeData, TreeData


class NodeDataTestCase(unittest.TestCase):
    def test_init(self):
        data = NodeData(ndim=1)
        self.assertEqual(0, data.size)
        self.assertEqual(NodeData.init_capacity, data.capacity)
        self.assertTrue(np.all(data.model == 0))

    def test_access(self):
        num_dims = 1
        num_entries = 4
        data = NodeData(ndim=num_dims)
        x = np.random.random((num_entries, num_dims))
        y = np.random.random(num_entries)
        for i in range(num_entries):
            data.add_data(x[i], y[i])
            stored_x, stored_y = data.get_data()
            self.assertTrue(np.all(stored_x == x[0:i+1, :]))
            self.assertTrue(np.all(stored_y == y[0:i+1]))

    def test_growing(self):
        data = NodeData(ndim=1)
        num_entries = 2*NodeData.init_capacity + 1
        xs = np.random.random((num_entries, 1))
        ys = np.random.random(num_entries)
        for x, y in zip(xs, ys):
            data.add_data(x, y)
        self.assertEqual(num_entries, data.size)
        self.assertTrue(np.all(xs == data.get_data()[0]))

    def test_shrinking(self):
        num_dims = 2
        num_entries = 2 * NodeData.init_capacity + 1
        data = NodeData(ndim=num_dims)
        xs = np.random.random((num_entries, num_dims))
        ys = np.random.random(num_entries)
        for x, y in zip(xs, ys):
            data.add_data(x, y)
        data.remove_oldest_data(num_entries=num_entries - 1)
        self.assertEqual(NodeData.init_capacity, data.capacity)
        self.assertTrue(np.all(xs[-1, :] == data.get_data()[0][0, :]))

    def test_add_multiple(self):
        num_dims = 2
        num_entries = 2 * NodeData.init_capacity + 1
        data = NodeData(ndim=num_dims)
        x = np.random.random((num_entries, num_dims))
        y = np.random.random(num_entries)
        data.add_data(x, y)
        self.assertEqual(num_entries, data.size)
        self.assertTrue(np.all(x == data.get_data()[0]))
        self.assertTrue(np.all(y == data.get_data()[1]))


class TreeDataTestCase(unittest.TestCase):
    def test_overwrite_history(self):
        leaf = Leaf()
        data = TreeData(num_dimensions=1, max_size=5)
        xs = np.random.random((10, 1))
        ys = np.random.random(10)
        for x, y in zip(xs, ys):
            data.add_data(leaf, x, y)
        self.assertTrue(np.all(data.get_data(leaf)[0] == xs[5:]))
        self.assertTrue(np.all(data.get_data(leaf)[1] == ys[5:]))

    def test_split(self):
        leaf = Leaf()
        data = TreeData(num_dimensions=1, max_size=20)
        xs = np.reshape(np.array([0, 1, 9, 8, 7, 2, 3, 4, 6, 5]), newshape=(10, 1))
        ys = np.random.random(10)
        for x, y in zip(xs, ys):
            data.add_data(leaf, x, y)
        left = Leaf()
        right = Leaf()
        data.split_data(leaf, left, right, 0, 5)
        self.assertTrue(np.all(data.get_data(left)[0] == xs[xs[:, 0] < 5, :]))
        self.assertTrue(np.all(data.get_data(right)[0] == xs[xs[:, 0] >= 5, :]))

    def test_newest_data(self):
        leaf = Leaf()
        data = TreeData(num_dimensions=1, max_size=100)
        xs = np.random.random((10, 1))
        ys = np.random.random(10)
        for x, y in zip(xs, ys):
            data.add_data(leaf, x, y)
        newest_x, newest_y = data.get_newest_data(4)
        self.assertTrue(np.all(newest_x == xs[6:, :]))
        self.assertTrue(np.all(newest_y == ys[6:]))

    def test_newest_several_leaves(self):
        lower = Leaf()
        upper = Leaf()
        data = TreeData(num_dimensions=1, max_size=100)
        xs = np.random.random((10, 1))
        ys = np.random.random(10)
        for x, y in zip(xs, ys):
            if x < 0.5:
                data.add_data(lower, x, y)
            else:
                data.add_data(upper, x, y)

        newest_x, newest_y = data.get_newest_data(10)
        self.assertTrue(np.all(xs == newest_x))
        self.assertTrue(np.all(ys == newest_y))

    def test_add_multiple(self):
        leaf = Leaf()
        data = TreeData(num_dimensions=2, max_size=5)
        xs = np.random.random((10, 2))
        ys = np.random.random(10)
        data.add_data(leaf, xs, ys)
        self.assertTrue(np.all(data.get_data(leaf)[0] == xs[5:, :]))
        self.assertTrue(np.all(data.get_data(leaf)[1] == ys[5:]))

    def test_model_setter(self):
        ndim = 5
        leaves = [Leaf() for _ in range(10)]
        models = [np.random.random(ndim + 1) for _ in leaves]
        x = np.random.random(ndim)
        y = 1
        data = TreeData(num_dimensions=ndim, max_size=len(leaves))
        for l, m in zip(leaves, models):
            data.add_data(l, x, y)
            data.set_model(l, m)

        for l, m in zip(leaves, models):
            self.assertTrue(np.all(data.get_model(l) == m))

from mpi4py import MPI
import numpy as np

from ret.message_passing import Communicator, TAG
from ret.worker import Worker


def test_update():
    comm = Communicator(MPI.COMM_WORLD, stationary_data=True)
    assert comm.size == 2
    if comm.is_manager:
        comm._log.open()
        leaf = "leaf"
        x = np.ones((10, 1))
        y = np.zeros(10)
        comm.try_update_leaf(leaf, x, y)
        while True:
            for result in comm.get_updates():
                if result.leaf == "leaf" and np.all(result.model == np.zeros(2)):
                    print("test_update: SUCCESS")
                else:
                    print("test_update: FAILED")
                comm._log.close()
                return
    else:
        task = comm.get_task()
        assert task[0] == TAG.UPDATE_LEAF
        model = np.zeros(2)
        comm.send_update(model)
        return


def test_no_split():
    comm = Communicator(MPI.COMM_WORLD, stationary_data=True)
    assert comm.size == 2
    if comm.is_manager:
        comm._log.open()
        leaf = "leaf"
        x = np.ones((10, 1))
        y = np.zeros(10)
        comm.try_update_leaf(leaf, x, y, split_allowed=False)
        while True:
            for result in comm.get_updates():
                if result.leaf == "leaf" and np.all(result.model == np.zeros(2)):
                    print("test_no_split: SUCCESS")
                else:
                    print("test_no_split: FAILED")
                comm._log.close()
                return
    else:
        task = comm.get_task()
        assert task[0] == TAG.UPDATE_NO_SPLIT
        model = np.zeros(2)
        comm.send_update(model)
        return


def test_split():
    comm = Communicator(MPI.COMM_WORLD, stationary_data=True)
    model = np.zeros(2)
    leaf = "leaf"
    tree = "tree"
    error = 0
    feature = 0
    threshold = 0.5
    assert comm.size == 2
    if comm.is_manager:
        comm._log.open()
        x = np.ones((10, 1))
        y = np.zeros(10)
        comm.try_update_leaf(leaf, x, y)
        while True:
            for result in comm.get_updates():
                if result.leaf == leaf and np.all(result.model == model) and\
                        result.feature == feature and result.threshold == threshold:
                    print("test_split: SUCCESS")
                else:
                    print("test_split: FAILED")
                comm._log.close()
                return
    else:
        task = comm.get_task()
        assert task[0] == TAG.UPDATE_LEAF
        comm.send_update(model, feature, threshold)
        return


def test_drift():
    tree = "some tree structure"
    data = "some data structure"
    comm = Communicator(MPI.COMM_WORLD)
    assert comm.size == 2
    if comm.is_manager:
        comm._log.open()
        x = np.ones((20, 2))
        y = np.zeros(20)
        comm.grow_new_tree(x, y)
        while True:
            result = comm.get_new_tree()
            if result is not None:
                alt_tree, alt_data = result
                if alt_tree == tree and alt_data == data:
                    print("test_drift: SUCCESS")
                else:
                    print(f"test_drift: FAILED")
                comm._log.close()
                return

    else:
        task = comm.get_task()
        assert task[0] == TAG.GROW_TREE
        comm.send_new_tree(tree, data)
        return


def test_stop():
    comm = Communicator(MPI.COMM_WORLD)
    assert comm.size == 2
    if comm.is_manager:
        comm._log.open()
        while not comm.stop():
            print("unable to stop other processes")
    else:
        task = comm.get_task()
        if task[0] == TAG.NO_JOB:
            print("test_stop: SUCCESS")
        else:
            print("test_stop: FAILED")


def test_drift_tree():
    comm = Communicator(MPI.COMM_WORLD)
    assert comm.size == 2
    if comm.is_manager:
        comm._log.open()
        x = np.random.random((20, 2))
        y = x[:, 0] + x[:, 1]
        comm.grow_new_tree(x, y)
        while True:
            result = comm.get_new_tree()
            if result is not None:
                tree, data = result
                for leaf in tree.iterate_leaves():
                    model = data.get_model(leaf)
                    if not np.allclose(model, [0, 1, 1]):
                        print("test_drift_tree: FAILED (wrong linear model)")
                        break
                    print("test_drift_tree: SUCCESS")
                comm._log.close()
                return

    else:
        task = comm.get_task()
        worker = Worker(significance_level=0.001, max_depth=1, max_size=task[1].size)
        assert task[0] == TAG.GROW_TREE
        tree, data = worker.build_tree(task[1], task[2])
        comm.send_new_tree(tree, data)
        return


if __name__ == "__main__":
    test_update()
    test_no_split()
    test_split()
    test_drift()
    test_drift_tree()
    test_stop()

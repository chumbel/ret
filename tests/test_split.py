import unittest
import numpy as np
from ret.splitter import Splitter


class SplitterTestCase(unittest.TestCase):
    def test_gauss(self):
        # generated form a uniform distribution
        x = np.array([0.04034914, 0.39779615, 0.88946492, 0.94629255, 0.87875662,
                      0.98198936, 0.97045828, 0.49621957, 0.82867143, 0.94374554,
                      0.39817872, 0.61792367, 0.12776917, 0.28905005, 0.06214385,
                      0.12254159, 0.76311036, 0.52924124, 0.0505781, 0.1742501])
        x = x.reshape((len(x), 1))
        # generated from a standard normal distribution
        error = np.array([0.07032353,  0.52139574,  1.91193523,  0.4111902, -0.88623693,
                          -0.70921578,  1.83193147, -0.52280601, -0.57047093, -0.7099294,
                          -0.5605618,  0.47566895, -0.23993151, -1.70513776, -1.32548386,
                          1.70490151,  1.00988381,  0.80679527,  0.55600393, -0.72315721])
        spltr = Splitter(0.01, len(x))
        self.assertIs(None, spltr.suggest_split(x, error))

    def test_constant_error(self):
        num_samples = 40
        spltr = Splitter(0.01, num_samples)
        x = np.random.random((num_samples, 2))
        error = np.full(num_samples, 1)
        split = spltr.suggest_split(x, error)
        self.assertIsNotNone(split)

    def test_gauss_2(self):
        x = np.array([[0.6803856, 0.51073663],
                      [0.27594706, 0.99332127],
                      [0.19680688, 0.20841693],
                      [0.65946669, 0.09966995],
                      [0.28986968, 0.90280465],
                      [0.40883156, 0.92732362],
                      [0.19935286, 0.22709533],
                      [0.25382503, 0.60012747],
                      [0.47839772, 0.94342812],
                      [0.71088929, 0.49362982],
                      [0.98575334, 0.53783462],
                      [0.66838915, 0.68007785],
                      [0.36696581, 0.7163687],
                      [0.39212273, 0.87360832],
                      [0.00274792, 0.02500874],
                      [0.38032428, 0.03351215],
                      [0.30896434, 0.41383088],
                      [0.48544347, 0.45568335],
                      [0.86265428, 0.11028109],
                      [0.56214456, 0.4548029],
                      [0.46761889, 0.33206452],
                      [0.15008946, 0.43158612],
                      [0.28010579, 0.50822157],
                      [0.11031041, 0.51754819]])
        errors = np.random.normal(0, 1, x.shape[0])
        spltr = Splitter(0.01, x.shape[0])
        split = spltr.suggest_split(x, errors)
        self.assertIsNone(split)

    def test_trump_sample(self):
        significance = 0.001
        with np.load("trump_sample.npz", "r") as data:
            x = data["coordinates"]
            errors = data["errors"]
        spltr = Splitter(significance, len(errors))
        num_cells = spltr._get_num_cells(len(errors))
        self.assertEqual(2, num_cells)
        split = spltr.suggest_split(x, errors)
        if split is None:
            self.assertTrue(True)
        else:
            self.assertTrue(split[0] < x.shape[1])

    def test_points_on_line(self):
        x = np.random.random((20, 4))
        x[:11, 0] = 0.
        errors = 2 * x[:, 0]
        split = Splitter(0.05, len(x)).suggest_split(x, errors)
        self.assertEqual(0, split[0])
        self.assertEqual(np.mean(x[:, 0]), split[1])

import numpy as np
import unittest

import pandas as pd

from ret.printing import create_dataframe, plot_1d, plot_2d_contour, animate_1d, animate_2d
from ret.worker import Worker
from ret.nodes import Branch, Leaf


class Plot1dTestCase(unittest.TestCase):
    def test_edges_linear(self):
        x = np.linspace(0, 1, 100)
        y = 0.5 * x
        x = x.reshape((100, 1))
        worker = Worker(significance_level=0.1, max_depth=10, max_size=x.size)
        tree, data = worker.build_tree(x, y)
        plot_1d(create_dataframe(tree, data), x, y)
        self.assertIsInstance(tree.root, Leaf)

    def test_edges_sin(self):
        x = np.linspace(0, np.pi, 100)
        y = np.sin(x)
        x = x.reshape((100, 1))
        worker = Worker(significance_level=0.05, max_depth=10, max_size=x.size)
        tree, data = worker.build_tree(x, y)
        plot_1d(create_dataframe(tree, data), x, y)
        self.assertIsInstance(tree.root, Branch)

    def test_edges_step(self):
        x = np.linspace(0, 1, 100)
        y = np.where(x < 0.7, 0., 1.)
        x = x.reshape((100, 1))
        worker = Worker(significance_level=0.05, max_depth=5, max_size=x.size)
        tree, data = worker.build_tree(x, y)
        table = create_dataframe(tree, data)
        plot_1d(table, x, y)
        self.assertIsInstance(tree.root, Branch)


class Plot2dContourTestCase(unittest.TestCase):

    def test_print_linear(self):
        x1, x2 = np.meshgrid(np.linspace(0, 1, 50), np.linspace(0, 1, 50))
        y_true = 0.5 * x1 - 2 * x2
        worker = Worker(significance_level=0.01, max_depth=5, max_size=x1.size)
        tree, data = worker.build_tree(np.column_stack((x1.flat, x2.flat)), y_true.flatten())
        plot_2d_contour(create_dataframe(tree, data), x1, x2, y_true)
        self.assertIsInstance(tree.root, Leaf)

    def test_step(self):
        n_points = 50
        x1, x2 = np.meshgrid(np.linspace(0, 1, n_points), np.linspace(0, 1, n_points))
        y_true = np.where(x1 + x2 < 1, 0, 1)
        worker = Worker(significance_level=0.01, max_depth=5, max_size=x1.size)
        tree, data = worker.build_tree(np.column_stack((x1.flat, x2.flat)), y_true.flatten())
        table = create_dataframe(tree, data)
        plot_2d_contour(table, x1, x2, y_true)

    def test_sin(self):
        n_points = 30
        x1, x2 = np.meshgrid(np.linspace(-np.pi, np.pi, n_points), np.linspace(-np.pi, np.pi, n_points))
        y_true = np.sin(x1 + x2)
        worker = Worker(significance_level=0.01, max_depth=5, max_size=x1.size)
        tree, data = worker.build_tree(np.column_stack((x1.flat, x2.flat)), y_true.flatten())
        table = create_dataframe(tree, data)
        plot_2d_contour(table, x1, x2, y_true)


class Anim1dTestCase(unittest.TestCase):
    def test_step(self): # noqa
        data = {"node": [0, 1, 2],
                "depth": [0, 1, 1],
                "is_leaf": [False, True, True],
                "parent": [pd.NA, 0, 0],
                "split_feature": [0, pd.NA, pd.NA],
                "split_threshold": [0.5, pd.NA, pd.NA],
                "alternative": [pd.NA, pd.NA, pd.NA],
                "model": [pd.NA, np.array([1, 0]), np.array([0, 0])]}
        table = pd.DataFrame(data).set_index("node")

        n_frames = 10

        x = np.linspace(-np.pi, np.pi, 50)
        t = np.linspace(0, 1, n_frames+1)

        def tables(n):
            for i in range(n):
                yield table

        animate_1d(tables(n_frames), x.reshape((50, 1)), lambda x, t: np.where(x < 0.5 + t, 1, 0), t)


class Anim2dTestCase(unittest.TestCase):
    def test_step(self): # noqa
        data = {"node": [0, 1, 2],
                "depth": [0, 1, 1],
                "is_leaf": [False, True, True],
                "parent": [pd.NA, 0, 0],
                "split_feature": [0, pd.NA, pd.NA],
                "split_threshold": [0.5, pd.NA, pd.NA],
                "alternative": [pd.NA, pd.NA, pd.NA],
                "model": [pd.NA, np.array([1, 0, 0]), np.array([0, 0, 0])]}
        table = pd.DataFrame(data).set_index("node")

        n_frames = 10

        x = np.linspace(-np.pi, np.pi, 50)
        t = np.linspace(0, 1, n_frames+1)

        def tables(n):
            for _ in range(n):
                yield table

        animate_2d(tables(n_frames), *np.meshgrid(x, x),
                   lambda x1, x2, t: np.where(x1 + x2 < 0.7 + t, 1, 0), t, n_frames)

import numpy as np


class WindTunnelMeasurements:
    """a wrapper for the data set provided in the file 'WindTunnel_Measurement.csv'"""

    def __init__(self, component="u"):
        if component not in ["u", "v", "w"]:
            raise ValueError("invalid component of velocity field, must be 'u', 'v' or 'w'")
        self.target_index = 5 + ["u", "v", "w"].index(component)
        self.data = np.loadtxt("WindTunnel_Measurement.csv", delimiter=",")

    @property
    def n_features(self):
        return 3

    @property
    def n_samples(self):
        return 34288

    def __iter__(self):
        for row in self.data:
            coordinates = {label: float(value) for label, value in zip(range(3), row[1:4])}
            target = float(row[self.target_index])
            yield coordinates, target


class FunctionDataset:
    """a wrapper for a synthetic data set generated from a user-defined function"""
    def __init__(self, function, num_dimensions=1, lower=0, upper=1, seed=12345):
        self.ndim = num_dimensions
        self.x_min = lower
        self.x_max = upper
        self.func = function
        self.rand_gen = np.random.default_rng(seed)

    @property
    def n_features(self):
        return self.ndim

    def take(self, num_samples):
        for _ in range(num_samples):
            coordinates = self.x_min + (self.x_max - self.x_min) * self.rand_gen.random(self.ndim)
            target = self.func(coordinates)
            yield {i: val for i, val in zip(range(self.ndim), coordinates)}, target


class PerturbedFunctionDataset(FunctionDataset):
    def __init__(self, function, num_dimensions=1, lower=0, upper=1, error_variance=0.001, seed=12345,):
        super().__init__(function=function, num_dimensions=num_dimensions, lower=lower, upper=upper, seed=seed)
        self.spread = error_variance

    def take(self, num_samples):
        for _ in range(num_samples):
            coordinates = self.x_min + (self.x_max - self.x_min) * self.rand_gen.random(self.ndim)
            target = self.func(coordinates) + self.rand_gen.normal(0, self.spread)
            yield {i: val for i, val in zip(range(self.ndim), coordinates)}, target


class FileDataset:
    def __init__(self, path_to_file: str):
        """create a new wrapper for a dataset located at `path_to_file`
        :param path_to_file: path to a csv file containing a list of data points,
            relative to the location of this script.

        The file should be comma-separated,
        contain labels in the first row and
        have the target value in the last column
        """
        self.data = np.loadtxt(path_to_file, delimiter=",", skiprows=1)
        with open(path_to_file) as f:
            labels = f.readline().rstrip("\n")
        self.labels = labels.split(sep=",")
    @property
    def n_features(self):
        return self.data.shape[1] - 1

    @property
    def n_samples(self):
        return self.data.shape[0]

    def __iter__(self):
        for row in self.data:
            coordinates = {label: float(value) for label, value in zip(self.labels[:-1], row[:-1])}
            target = float(row[-1])
            yield coordinates, target


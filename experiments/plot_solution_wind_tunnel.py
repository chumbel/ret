import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import pickle as pkl

from ret.printing import predict, predict_edges, predict_edges_in_section, get_corners_from_boundaries

# limits are rounded up / down in last digit to ensure that all values are contained in the limits
min_coordinates = [0.68000000, -1.30006100, -0.05002707]
max_coordinates = [0.93003864, -0.80000002, 0.22003166]

min_values = [-11.4835568, -6.52120925, -6.78829289]
max_values = [-2.10715293, 4.98428060, 4.65464879]


def plot_cell_edges(table, component):
    predictions, cells = predict_edges(table, min_coordinates, max_coordinates)

    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')
    for cell in cells:
        corners = get_corners_from_boundaries(cell)
        # front side
        idx = np.asarray([0, 1, 3, 2, 0], dtype=int)
        ax.plot(corners[idx, 0], corners[idx, 1], corners[idx, 2], color="blue", linewidth=0.5)
        # back side
        idx += 4
        ax.plot(corners[idx, 0], corners[idx, 1], corners[idx, 2], color="blue", linewidth=0.5)
        # right side
        idx = np.asarray([0, 4, 5, 1, 0], dtype=int)
        ax.plot(corners[idx, 0], corners[idx, 1], corners[idx, 2], color="blue", linewidth=0.5)
        # left side
        idx += 2
        ax.plot(corners[idx, 0], corners[idx, 1], corners[idx, 2], color="blue", linewidth=0.5)

    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_zlabel("z")
    plt.show()


def plot_section(table, component, coordinate, position=0.5):
    directions = ["u", "v", "w"]
    direction = directions.index(component)
    section_value = min_coordinates[coordinate] + position * (max_coordinates[coordinate] - min_coordinates[coordinate])
    predictions, cells = predict_edges_in_section(table, min_coordinates, max_coordinates, coordinate, section_value)
    fig = plt.figure()
    ax = fig.add_subplot()
    y_min = 2*min_values[direction]
    y_max = 2*max_values[direction]
    for prediction, cell in zip(predictions, cells):
        cell = np.delete(cell, coordinate, 0)
        try:
            img = ax.pcolormesh(cell[0], cell[1], prediction.reshape(2, 2),
                                shading="gouraud", vmin=y_min, vmax=y_max)
        except:  # noqa
            pass
        # draw borders of the cell
        ax.vlines(cell[0], ymin=[cell[1, 0]] * 2, ymax=[cell[1, 1]] * 2, color="grey", linewidth=0.5)
        ax.hlines(cell[1], xmin=[cell[0, 0]] * 2, xmax=[cell[0, 1]] * 2, color="grey", linewidth=0.5)
    plt.colorbar(mappable=img, label="velocity / $\mathrm{m} \, \mathrm{s}^{-1}$")
    coordinates = ["x", "y", "z"]
    coordinate_label = coordinates.pop(coordinate)
    direction_label = directions[direction]
    ax.set_xlabel(coordinates[0])
    ax.set_ylabel(coordinates[1])
    ax.set_title(f"Approximation of Velocity Component ${direction_label}$"
                 f" at ${coordinate_label} = {section_value:.2}$")
    plt.savefig(f"result_{direction_label}_{coordinate_label}_{section_value:.2}.png")


def plot_all_sections(table, component, n_sections, scale_colour=2):
    directions = ["u", "v", "w"]
    direction = directions.index(component)

    fig, axs = plt.subplots(nrows=3, ncols=n_sections, figsize=(8, 8),
                            sharex="row", sharey="row", constrained_layout=True)
    y_min = scale_colour*min_values[direction]
    y_max = scale_colour*max_values[direction]
    for coordinate in range(3):
        coordinates = ["x", "y", "z"]
        coordinate_label = coordinates.pop(coordinate)
        section_values = (min_coordinates[coordinate]
                          + np.arange(1, n_sections + 1) / (n_sections + 1)
                          * (max_coordinates[coordinate] - min_coordinates[coordinate]))
        for sec_val, i in zip(section_values, range(n_sections)):
            ax = axs[coordinate, i]
            predictions, cells = predict_edges_in_section(table, min_coordinates, max_coordinates, coordinate, sec_val)
            for prediction, cell in zip(predictions, cells):
                cell = np.delete(cell, coordinate, 0)
                try:
                    img = ax.pcolormesh(cell[0], cell[1], prediction.reshape(2, 2),
                                        shading="gouraud", vmin=y_min, vmax=y_max)
                except:  # noqa
                    pass
                # draw borders of the cell
                ax.vlines(cell[0], ymin=[cell[1, 0]] * 2, ymax=[cell[1, 1]] * 2, color="grey", linewidth=0.5)
                ax.hlines(cell[1], xmin=[cell[0, 0]] * 2, xmax=[cell[0, 1]] * 2, color="grey", linewidth=0.5)

            ax.set_title(f"${coordinate_label} = {sec_val:1.2}$")
        axs[coordinate, 0].set_ylabel(f"{coordinates[1]}")
        axs[coordinate, n_sections // 2].set_xlabel(f"{coordinates[0]}")
    fig.colorbar(img, ax=axs.ravel().tolist(), label="velocity / $\mathrm{m} \, \mathrm{s}^{-1}$")
    plt.suptitle(f"Approximation of Velocity Component ${component}$")
    plt.savefig(f"plot_approx_{component}.png", dpi=300)


def plot_section_3d(section_coordinate, position, path, n_points=10, max_value=20):
    cmap = mpl.colormaps["viridis"]
    min_margin = [0.68, -1.2, 0]
    max_margin = [0.93, -0.9, 0.17]
    # get data at section
    scaling = 0.002
    coordinates = [0, 1, 2]
    coordinate_labels = ["x", "y", "z"]
    section_value = (min_coordinates[section_coordinate]
                     + position * (max_coordinates[section_coordinate] - min_coordinates[section_coordinate]))
    x, y, z = np.meshgrid(*[np.linspace(min_margin[c], max_margin[c], n_points) if c is not section_coordinate
                            else section_value for c in coordinates])
    feature_values = np.column_stack((x.flat, y.flat, z.flat))
    prediction = []
    for component in ["u", "v", "w"]:
        table = pkl.load(open(f"{path}/table_wind_tunnel_{component}.pkl", "rb"))
        prediction.append(scaling * predict(table, feature_values).reshape(x.shape))

    realistic_positions = np.logical_and.reduce([(np.abs(p) < max_value*scaling) for p in prediction])
    realistic_values = [p[realistic_positions] for p in prediction]

    lengths = np.sqrt(np.sum([np.square(p) for p in prediction], axis=0))[realistic_positions].ravel()
    len_range = np.max(lengths) - np.min(lengths)
    print(np.max(lengths), np.min(lengths))
    colors = [cmap(int(255 - 255*(len_range - l) / len_range)) for l in lengths]
    fig = plt.figure()
    ax = fig.add_subplot(projection="3d")
    vec_field = ax.quiver(x[realistic_positions], y[realistic_positions], z[realistic_positions],
                          *realistic_values, color=colors)
    ax.scatter(x[~realistic_positions], y[~realistic_positions], z[~realistic_positions], marker="x", color="grey")
    ax.set_xlabel("x")
    ax.set_xticks([round(section_value, 4)])
    ax.set_ylabel("y")
    ax.set_zlabel("z")
    plt.colorbar(vec_field, label="magnitude from min to max", shrink=0.8)
    plt.title(f"Velocity Field at ${coordinate_labels[section_coordinate]} = {section_value:.4}$ ")
    plt.show()


def plot_path(n):
    data = np.loadtxt("WindTunnel_Measurement.csv", delimiter=",")
    coordinates = data[:, 1:4]
    fig = plt.figure()
    ax = fig.add_subplot(projection="3d")
    ax.plot(coordinates[:n, 0], coordinates[:n, 1], coordinates[:n, 2])
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_zlabel("z")
    plt.show()


if __name__ == "__main__":
    folder = "../../results_wind_tunnel_7"
    # plot_section_3d(0, 0.5, folder, 18, 30)
    # for c in ["u", "v", "w"]:
    #     plot_all_sections(pkl.load(open(f"{folder}/table_wind_tunnel_{c}.pkl", "rb")), c, 3)
    plot_path(2000)
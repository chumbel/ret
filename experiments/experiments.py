import argparse
import sys

import numpy as np
from mpi4py import MPI

from river.datasets.synth import Friedman, FriedmanDrift
from river.datasets import TrumpApproval
from river.preprocessing import StandardScaler
from river import metrics

from ret.message_passing import Communicator
from ret.regression_element_tree import RegressionElementTree
from ret.worker import Worker

from datasets import WindTunnelMeasurements, PerturbedFunctionDataset, FileDataset


def get_arguments() -> dict:
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--dataset", required=True)
    parser.add_argument("-c", "--sample_count", type=int)
    parser.add_argument("-e", "--seed", type=int, default=12345)
    parser.add_argument("-l", "--log")
    parser.add_argument("-p", "--printing_interval", type=int, default=sys.maxsize)
    parser.add_argument("-r", "--store_predictions")
    parser.add_argument("-t", "--store_tree")
    parser.add_argument("-a", "--store_data_points")
    parser.add_argument("-w", "--data_window", type=int, default=5000)
    parser.add_argument("-v", "--evaluation_window", type=int, default=1000)
    parser.add_argument("-m", "--max_depth", type=int, default=30)
    parser.add_argument("-s", "--significance_level", type=float, default=0.001)
    parser.add_argument("--sequential", action="store_true", default=False)
    parser.add_argument("--no_limit_tasks", action="store_true", default=False)
    parser.add_argument("--stationary", action="store_true", default=False)
    parser.add_argument("--record_time", action="store_true", default=False)
    parser.add_argument("--scale_data", action="store_true", default=False)

    return vars(parser.parse_args())


def get_dataset(name, sample_count, random_seed):
    # use datasets as in river benchmark: https://riverml.xyz/0.13.0/benchmarks/
    if sample_count is not None:
        if name[:3] == "sin":
            feature_count = int(name[3:])
            data = PerturbedFunctionDataset(lambda coordinates: np.prod(np.sin(coordinates)),
                                            feature_count, upper=2 * np.pi, seed=random_seed).take(sample_count)
        else:
            raise ValueError("invalid options for data set")

    else:
        if name == "Friedman":
            sample_count = 7000
            data = Friedman(random_seed).take(sample_count)
            feature_count = 10
        elif name == "FriedmanGSG":
            sample_count = 10000
            pos = (3500, 7000)
            window = 1000
            data = FriedmanDrift("gsg", pos, window, random_seed).take(sample_count)
            feature_count = 10
        elif name == "FriedmanLEA":
            sample_count = 10000
            pos = (2000, 5000, 8000)
            data = FriedmanDrift("lea", pos, seed=random_seed).take(sample_count)
            feature_count = 10
        elif name == "TrumpApproval":
            data = TrumpApproval()
            feature_count = data.n_features
            sample_count = data.n_samples
        elif name[:10] == "WindTunnel":
            data = WindTunnelMeasurements(name[10])
            feature_count = data.n_features
            sample_count = data.n_samples
        else:
            data = FileDataset(name)
            feature_count = data.n_features
            sample_count = data.n_samples
    return data, feature_count, sample_count


if __name__ == "__main__":
    args = get_arguments()
    comm = Communicator(MPI.COMM_WORLD,
                        stationary_data=args.pop("stationary"),
                        max_capacity=MPI.COMM_WORLD.Get_size() if not args.pop("no_limit_tasks") else None,
                        logfile=args.pop("log"))
    if comm.is_manager:
        data_name = args.pop("dataset")
        n_samples = args.pop("sample_count")
        seed = args.pop("seed")
        table_file = args.pop("store_tree")
        data_file = args.pop("store_data_points")
        error_file = args.pop("store_predictions")
        sequential = args.pop("sequential")
        scale = args.pop("scale_data")
        print_interval = args.pop("printing_interval")

        dataset, n_features, n_samples = get_dataset(data_name, n_samples, seed)

        print(f"arguments for RET: {args}")

        tree = RegressionElementTree(comm, n_features, **args)
        if scale:
            model = (StandardScaler(with_std=True) | tree)
        else:
            model = tree

        if error_file is not None:
            error_table = np.empty((n_samples, 2))

        error = metrics.MAE()

        timing = None

        try:
            for x, y in dataset:
                prediction = model.predict_one(x)
                error.update(y, prediction)
                model.learn_one(x, y)
                if sequential:
                    tree.complete_tasks()
                if error_file is not None:
                    error_table[tree.num_samples - 1] = [y, prediction]  # noqa
                if tree.num_samples % print_interval == 0:
                    print(f"{tree.num_samples}, {error.get()}")
                    if table_file is not None:
                        tree.to_dataframe().to_pickle(f"{table_file}_{model.num_samples}.pkl")
                    if data_file is not None:
                        data = tree.data.get_newest_data(model.data.max_size)[0]
                        np.save(f"{data_file}_{tree.num_samples}", data)

            timing = tree.end_computation()
        except Exception as e: # noqa
            print(f"some error was encountered on manager process, "
                  f"which caused all processes to abort: {e.__class__.__name__} {e}")
            comm._comm.Abort() # noqa

        print(f"Total error after {tree.num_samples} samples: {error.get()}")

        if timing is not None:
            print("process_rank, total_time, working_time")
            for rank, total, working in zip(timing["rank"], timing["total"], timing["working"]):
                print(rank, total, working)
        if table_file is not None:
            tree.to_dataframe().to_pickle(f"{table_file}_{tree.num_samples}.pkl")
        if data_file is not None:
            data = tree.data.get_newest_data(model.data.max_size)[0]
            np.save(f"{data_file}_{tree.num_samples}", data)
        if error_file is not None:
            np.save(error_file, error_table)
    else:
        try:
            worker = Worker(comm)
            worker.run()
        except Exception as e: # noqa
            print(f"some error was encountered on worker process {comm.rank}, "
                  f"which caused all processes to abort: {e.__class__.__name__} {e}")
            comm._comm.Abort() # noqa

#ifndef _CHI2_TEST_
#define _CHI2_TEST_

#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <numeric>


using index_t = unsigned int;
using count_t = unsigned int;
using stat_t = double;

/** compute the chi-square test value using given categories
 * 
 * This function constructs a contingency table from the given row and 
 * column data. It assumes that the pair ('rows', 'columns') provides
 * the categories of a list of data points.
 * 
 * 'row_freq' and 'col_freq' provide the marginal frequencies of the
 * different categories, which is equal to the number of entries in 'rows'/'columns'
 * with the corresponding index. It is assumed (but not checked!) that
 * the frequencies are correctly summarising the data points.
 * 
 * the dimensions of 'rows'/'columns' and 'row_freq'/'col_freq' each 
 * have to be identical.
 *
 * @param rows row indices of all data points
 * @param row_freq marginal frequencies of the rows
 * @param columns column indices of all data points
 * @param column_freq marginal frequencies of the columns
 * @return value of the chi-square test
 */
stat_t compute_test_value(const std::vector<index_t> &rows, const std::vector<count_t> &row_freq,
                          const std::vector<index_t> &columns, const std::vector<count_t> &column_freq) {
    count_t k = row_freq.size();  // number of cells
    count_t n = rows.size();  // number of data points
    if (k != column_freq.size()) {
        std::cerr << "UNEQUAL NUMBER OF CATEGORIES IN CONTINGENCY TABLE\n";
        return 0;
    }
    if (n != columns.size()) {
        std::cerr << "UNEQUAL NUMBER OF ENTRIES IN ROWS AND COLUMNS\n";
        return 0;
    }
    // initialise contingency table with zeros
    std::vector<count_t> table(k * k, 0);
    // iterate through all entries of rows, columns to fill contingency table
    for (index_t i = 0; i < n; ++i) {
        table[rows[i] + k * columns[i]] += 1;
    }
    stat_t test_value = 0;
    // compute value of test statistic
    for (index_t i = 0; i < k; ++i) {
        for (index_t j = 0; j < k; ++j) {
            stat_t expected = stat_t(row_freq[i] * column_freq[j]) / n;
            test_value += std::pow(table[i + k * j] - expected, 2) / expected;
        }
    }
    return test_value;
}


/** distribute values into equifrequent cells
 * 
 * This function sorts and then partitions the values in the range from 'first'
 * to 'last' into 'num_cells' segments of approximately equal size.
 * Identical values must be part of the same segment ("cell").
 * If 'fixed_cells' is set to 'false' (the default) the function may 
 * reduce the number of cells to the number of unique values.
  *
  * @tparam Iterator
  * @param first
  * @param last
  * @param num_cells number of segments
  * @param fixed_cells whether or not the function is allowed to reduce the number of segments
  * @return
  */
template<class Iterator>
std::pair<std::vector<index_t>, std::vector<count_t>>
distribute_values(Iterator first, Iterator last, count_t num_cells, bool fixed_cells = false,
                  typename std::iterator_traits<Iterator>::difference_type stride = 1) {
    // check iterator type
    static_assert(std::is_base_of_v<std::random_access_iterator_tag,
            typename std::iterator_traits<Iterator>::iterator_category>);

    using value_t = typename std::iterator_traits<Iterator>::value_type;
    const auto num_values = std::distance(first, last) / stride;

    // create a result object to hold cell numbers for each value and marginal frequencies
    auto result = std::make_pair(std::vector<index_t>(num_values), std::vector<count_t>());
    auto &cells = result.first;
    auto &marginal_frequencies = result.second;

    // special cases:
    // - if no data, return empty result
    if (num_values <= 0) return result;

    // - if only one cell, all values belong to this cell
    if (num_cells <= 1) {
        std::fill(cells.begin(), cells.end(), 0);
        marginal_frequencies.push_back(num_values);
        return result;
    }

    // create a vector of (value, position) pairs
    std::vector<std::pair<value_t, index_t>> indexed_values;
    indexed_values.reserve(num_values);
    index_t position = 0;
    for (auto it = first; it < last; it += stride) {
        indexed_values.emplace_back(*it, position++);
    }

    // sort according to value
    std::sort(indexed_values.begin(), indexed_values.end(), [](auto left, auto right) {
        return left.first < right.first;
    });

    // find unique values
    std::vector<value_t> unique;
    unique.reserve(indexed_values.size());
    value_t current = indexed_values.front().first;
    unique.push_back(current);
    std::for_each(indexed_values.begin(), indexed_values.end(), [&unique, &current](auto p) {
        if (p.first != current) {
            current = p.first;
            unique.push_back(current);
        }
    });

    const count_t num_unique = unique.size();

    // if only one value, test can't produce a split
    // if less values than cells, and we are not allowed to reduce the number of cells
    // just return one cell, since we won't do a split in this case
    if (num_unique == 1 || (fixed_cells && num_unique < num_cells)) {
        std::fill(cells.begin(), cells.end(), 0);
        marginal_frequencies.push_back(num_values);
        return result;
    }

    num_cells = std::min(num_cells, num_unique);
    marginal_frequencies.reserve(num_cells);

    // find boundaries of cells
    const count_t unique_per_cell = num_unique / num_cells;

    // go through sorted values and set corresponding cell number
    index_t cell_number;
    auto unique_it = unique.begin();
    auto sorted_it = indexed_values.begin();
    for (cell_number = 0; cell_number < num_cells - 1; ++cell_number) {
        unique_it += unique_per_cell + count_t(cell_number < num_unique % num_cells);
        count_t count = 0;
        while (sorted_it->first < *unique_it) {
            cells[sorted_it->second] = cell_number;
            ++sorted_it;
            ++count;
        }
        marginal_frequencies.push_back(count);
    }
    marginal_frequencies.push_back(std::distance(sorted_it, indexed_values.end()));
    std::for_each(sorted_it, indexed_values.end(), [&cells, cell_number](auto p) { cells[p.second] = cell_number; });
    return result;
}

/** compute values of pairwise chi2 independence tests
 *
 * this function takes as inputs several error values and
 * corresponding locations, which are given in a contiguous collection sorted by dimension.
 * errors = {e1, ... , eN}, coordinates = {x1, ... xN, y1, ... yN, ...}
 * the function computes pairwise independence of errors and each spatial dimension using a chi-square
 * independence test with a prescribed number of categories ('cells').
 *
 * @tparam Iterator
 * @param errors_first
 * @param errors_last
 * @param coordinates_first
 * @param coordinates_last
 * @param num_cells number of categories used in the chi-square test
 * @return vector containing the test values
 */
template<class Iterator>
std::vector<stat_t>
compute_chisquare(const Iterator errors_first, const Iterator errors_last, const Iterator coordinates_first,
                  const Iterator coordinates_last, count_t num_cells) {
    // initialise result
    std::vector<stat_t> stats;

    // get dimensions
    count_t num_elements = std::distance(errors_first, errors_last);
    count_t num_coords = std::distance(coordinates_first, coordinates_last);
    count_t num_dimensions = num_coords / num_elements;
    if (num_dimensions * num_elements != num_coords) {
        std::cerr << "SIZES OF ERRORS AND COORDINATES DO NOT MATCH\n";
        return stats;
    }
    if (num_elements < num_cells) {
        std::cerr << "TOO FEW VALUES FOR GIVEN NUMBER OF CELLS\n";
        return stats;
    }
    stats.reserve(num_dimensions);
    // get rows of errors
    auto [rows, row_freq] = distribute_values(errors_first, errors_last, num_cells);
    // update num_cells if less rows
    num_cells = std::min(count_t(row_freq.size()), num_cells);
    // for each dimension, get columns of coordinate values and compute test statistic
    auto column_start = coordinates_first;
    auto column_end = coordinates_last;
    for (index_t d = 0; d < num_dimensions; ++d) {
        auto [cols, col_freq] = distribute_values(column_start, column_end, num_cells, true, num_dimensions);
        // if not all cells could be filled, skip
        if (col_freq.size() != num_cells) stats.push_back(0.);
        else stats.push_back(compute_test_value(rows, row_freq, cols, col_freq));
        ++column_start;
        ++column_end;
    }
    return stats;
}


#endif // _CHI2_TEST_

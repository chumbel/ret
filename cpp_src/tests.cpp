#include <algorithm>
#include <vector>
#include <random>

#include "chi2_test.hpp"
#include "gtest/gtest.h"

namespace {

  // Testing of function "distribute_values"

  TEST(DistributeValueTest, AllDifferent) {
    // setup
    int num_values = 1000;
    int num_cells = 10;
    std::default_random_engine rand_gen(42);
    std::vector<double> values(num_values);
    double init = 0.;
    std::generate(values.begin(), values.end(), [&init] { return init += 0.1; });
    std::shuffle(values.begin(), values.end(), rand_gen);

    // compute cells and value counts
    auto [cells, counts] = distribute_values(values.begin(), values.end(), num_cells);

    // check
    EXPECT_EQ(num_values, cells.size());
    for (auto c: cells) EXPECT_TRUE(0 <= c && c < num_cells);
    EXPECT_EQ(num_cells, counts.size());
    for (auto c: counts) EXPECT_EQ(num_values / num_cells, c);
  }

  TEST(DistributeValueTest, AllDifferentHuge) {
    // setup
    int num_values = 1000000;
    int num_cells = 1000;
    std::default_random_engine rand_gen(42);
    std::vector<double> values(num_values);
    double init = 0.;
    std::generate(values.begin(), values.end(), [&init] { return init += 0.1; });
    std::shuffle(values.begin(), values.end(), rand_gen);

    // compute cells and value counts
    auto [cells, counts] = distribute_values(values.begin(), values.end(), num_cells);

    // check
    EXPECT_EQ(num_values, cells.size());
    for (auto c: cells) EXPECT_TRUE(0 <= c && c < num_cells);
    EXPECT_EQ(num_cells, counts.size());
    for (auto c: counts) EXPECT_EQ(num_values / num_cells, c);
  }

  TEST(DistributeValueTest, AllEqual) {
    // setup
    int num_values = 100;
    int num_cells = 10;
    std::vector<double> values(num_values, 1);

    // compute cells and value counts
    auto [cells, counts] = distribute_values(values.begin(), values.end(), num_cells);

    // check: only one cell
    EXPECT_EQ(num_values, cells.size());
    for (auto c: cells) EXPECT_EQ(0, c);
    EXPECT_EQ(1, counts.size());
    EXPECT_EQ(num_values, counts[0]);
  }

  TEST(DistributeValueTest, ExactNumCells) {
    // setup
    int num_cells = 10;
    std::vector<int> values;
    for (int i = 1; i <= num_cells; ++i) {
      for (int j = 0; j < i; ++j) values.push_back(i);
    }
    int num_values = values.size();
    std::default_random_engine rand_gen(42);
    std::shuffle(values.begin(), values.end(), rand_gen);

    // compute cells and value counts
    auto [cells, counts] = distribute_values(values.begin(), values.end(), num_cells);

    // check
    EXPECT_EQ(num_values, cells.size());
    for (auto c: cells) EXPECT_TRUE(0 <= c && c < num_cells);
    EXPECT_EQ(num_cells, counts.size());
    for (int i = 0; i < num_cells; ++i) {
      EXPECT_EQ(i + 1, counts[i]);
    }
  }

  TEST(DistributeValueTest, FewUniqueFixedSize) {
    // setup
    int num_cells = 10;
    std::vector<int> values;
    for (int i = 1; i <= num_cells / 2; ++i) {
      for (int j = 0; j < i; ++j) values.push_back(i);
    }
    int num_values = values.size();
    std::default_random_engine rand_gen(42);
    std::shuffle(values.begin(), values.end(), rand_gen);

    // compute cells and value counts
    auto [cells, counts] = distribute_values(values.begin(), values.end(), num_cells, true);

    // check: only one cell
    EXPECT_EQ(num_values, cells.size());
    for (auto c: cells) EXPECT_EQ(0, c);
    EXPECT_EQ(1, counts.size());
    EXPECT_EQ(num_values, counts[0]);
  }

  TEST(DistributeValueTest, FewUniqueSorted) {
    // setup
    int num_cells = 10;
    int num_unique = num_cells / 2;
    std::vector<int> values;
    for (int i = 1; i <= num_unique; ++i) {
      for (int j = 0; j < i; ++j) values.push_back(i);
    }
    int num_values = values.size();

    // compute cells and value counts
    auto [cells, counts] = distribute_values(values.begin(), values.end(), num_cells);

    // check: only one cell
    EXPECT_EQ(num_values, cells.size());
    for (auto c: cells) EXPECT_TRUE(0 <= c && c < num_unique);
    EXPECT_EQ(num_unique, counts.size());
    for (int i = 0; i < num_unique; ++i) {
      EXPECT_EQ(i + 1, counts[i]);
    }
  }

  TEST(DistributeValueTest, Empty) {
    // setup
    int num_cells = 10;
    int num_values = 0;
    std::vector<double> values(num_values);

    // compute cells and value counts
    auto [cells, counts] = distribute_values(values.begin(), values.end(), num_cells);

    // check: no cell
    EXPECT_EQ(0, cells.size());
    EXPECT_EQ(0, counts.size());
  }

  // Testing of function "compute_test_value"

  TEST(ComputeTestValueTest, Empty) {
    std::vector<unsigned> empty;
    EXPECT_EQ(0., compute_test_value(empty, empty, empty, empty));
  }

  TEST(ComputeTestValueTest, UniformSorted) {
    // setup
    int num_values = 100;
    int num_cells = 10;
    std::vector<unsigned> rows, cols;
    std::vector<unsigned> frequencies(num_cells, num_values / num_cells);
    rows.reserve(num_values);
    for (int i = 0; i < num_cells; ++i) {
      for (int j = 0; j < num_values / num_cells; ++j) {
        rows.push_back(i);
      }
    }
    cols.reserve(num_values);
    for (int i = 0; i < num_values / num_cells; ++i) {
      for (int j = 0; j < num_cells; ++j) {
        cols.push_back(j);
      }
    }

    // check: totally independent should have value zero
    EXPECT_EQ(0., compute_test_value(rows, frequencies, cols, frequencies));

  }
  
  TEST(ComputeTestValueTest, Correlated) {
    // setup: contingency table with 10 categories and 10s on the diagonal
    int num_values = 100;
    int num_cells = 10;
    std::vector<unsigned> cells;
    std::vector<unsigned> frequencies(num_cells, num_values / num_cells);
    cells.reserve(num_values);
    for (int i = 0; i < num_cells; ++i) {
      for (int j = 0; j < num_values / num_cells; ++j) {
        cells.push_back(i);
      }
    }
    std::default_random_engine rand_gen(42);
    std::shuffle(cells.begin(), cells.end(), rand_gen);
    
    // check: compare to value computed with scipy.stats.chi2_contingency
    EXPECT_DOUBLE_EQ(compute_test_value(cells, frequencies, cells, frequencies), 900.);
    
  }

  TEST(ComputeTestValueTest, UpperTriangle) {
    // setup: a contingecy table with 10 categories and 10s in the upper triangle
    int num_cells = 10;
    unsigned entry = 10;
    // fill marginal frequencies
    std::vector<unsigned> row_frequencies(num_cells);
    std::vector<unsigned> col_frequencies(num_cells);
    for (int i = 1; i <= num_cells; ++i) {
      row_frequencies[i-1] = entry * i;
      col_frequencies[num_cells - i] = entry * i;
    }
    // fill indices for rows
    unsigned num_values = std::accumulate(row_frequencies.begin(), row_frequencies.end(), 0u);
    std::vector<unsigned> rows;
    rows.reserve(num_values);
    for (int i = 0; i < num_cells; ++i) {
      for (int j = 0; j < row_frequencies[i]; ++j) {
        rows.push_back(i);
      }
    }
    // fill indices for cols
    std::vector<unsigned> cols;
    cols.reserve(num_values);
    for (int i = 0; i < num_cells; ++i) {
      for (int j = 0; j <= i; ++j) {
          for (int k = 0; k < entry; ++k) {
            cols.push_back(j);
          }
      }
    }

    // check: compare to value computed with scipy.stats.chi2_contingency
    EXPECT_DOUBLE_EQ(compute_test_value(rows, row_frequencies, cols, col_frequencies), 302.37225214159736);
  }

  TEST(ComputeTestValueTest, Random0) {
    // setup: a randomly generated contingency table
    std::vector<std::vector<unsigned>> table = {
        {5, 7, 3, 8},
        {9, 6, 8, 5},
        {7, 4, 6, 8},
        {5, 9, 5, 6}
    };
    std::vector<unsigned> row_frequencies = {23, 28, 25, 25};
    std::vector<unsigned> col_frequencies = {26, 26, 22, 27};

    std::vector<unsigned> rows;
    rows.reserve(101);
    std::vector<unsigned> cols;
    cols.reserve(101);

    for (unsigned i = 0; i < 4; ++i) {
      for (unsigned j = 0; j < 4; ++j) {
        for (unsigned k = 0; k < table[i][j]; ++k) {
          rows.push_back(i);
          cols.push_back(j);
        }
      }
    }

    // check: compare to value computed with scipy.stats.chi2_contingency
    EXPECT_DOUBLE_EQ(compute_test_value(rows, row_frequencies, cols, col_frequencies), 6.504934976456716);
  }

  TEST(ComputeTestValueTest, Random1) {
    // setup: a randomly generated contingency table
    std::vector<std::vector<unsigned>> table = {
        {12,  7, 13,  8},
        { 7, 11,  8, 15},
        { 7, 14, 13,  8},
        {15,  9, 11, 14}
    };

    std::vector<unsigned> row_frequencies = {40, 41, 42, 49};
    std::vector<unsigned> col_frequencies = {41, 41, 45, 45};

    std::vector<unsigned> rows;
    rows.reserve(172);
    std::vector<unsigned> cols;
    cols.reserve(172);

    for (unsigned i = 0; i < 4; ++i) {
      for (unsigned j = 0; j < 4; ++j) {
        for (unsigned k = 0; k < table[i][j]; ++k) {
          rows.push_back(i);
          cols.push_back(j);
        }
      }
    }

    // check: compare to value computed with scipy.stats.chi2_contingency
    EXPECT_DOUBLE_EQ(compute_test_value(rows, row_frequencies, cols, col_frequencies), 11.443645042655838);
  }

  TEST(ComputeTestValueTest, One) {
    // setup: a contingency table with one entry
    std::vector<unsigned> frequencies = {15};
    std::vector<unsigned> cells(15, 0);

    EXPECT_DOUBLE_EQ(compute_test_value(cells, frequencies, cells, frequencies), 0.);
  }

  // Testing of function "compute_chisquare"

  TEST(ComputeChisquare, NormalError1D) {
    std::vector<float> x = {0.04034914, 0.39779615, 0.88946492, 0.94629255, 0.87875662,
                            0.98198936, 0.97045828, 0.49621957, 0.82867143, 0.94374554,
                            0.39817872, 0.61792367, 0.12776917, 0.28905005, 0.06214385,
                            0.12254159, 0.76311036, 0.52924124, 0.05057810, 0.17425010};
    std::vector<float> e = { 0.07032353,  0.52139574,  1.91193523,  0.41119020, -0.88623693,
                            -0.70921578,  1.83193147, -0.52280601, -0.57047093, -0.70992940,
                            -0.56056180,  0.47566895, -0.23993151, -1.70513776, -1.32548386,
                             1.70490151,  1.00988381,  0.80679527,  0.55600393, -0.72315721};

    auto statistic = compute_chisquare(e.begin(), e.end(), x.begin(), x.end(), 3);
    double expected = 3.038548752834467;

    // check: compare with result obtained with scipy's chi2_contingency
    EXPECT_DOUBLE_EQ(expected, statistic[0]);
  }
}

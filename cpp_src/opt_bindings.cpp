#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>

#include "chi2_test.hpp"

namespace py = pybind11;


PYBIND11_MODULE(opt_bindings, m) {
    m.doc() = "optimised implementation of chi2 test"; // module docstring

    m.def("chi2",
          [](const py::array_t<double, py::array::c_style>& err,
             const py::array_t<double, py::array::c_style>& coords,
             const count_t n_cells) {
                auto err_info = err.request();
                auto coord_info = coords.request();
                
                auto err_begin = (double const*)err_info.ptr;
                auto err_end = err_begin + err_info.size;
                auto coords_begin = (double const*)coord_info.ptr;
                auto coords_end = coords_begin + coord_info.size;
                
                return compute_chisquare(
                    err_begin,
                    err_end,
                    coords_begin,
                    coords_end,
                    n_cells);

//                py::list result;
//                for (auto s: statistic) result.append(s);
//                return result;
          },
          "compute chi2 test statistic for a set of errors and several features");
}

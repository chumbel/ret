# Online Regression Element Trees

## Description
This project is part of my BSc Thesis "Tree-based Regression Method for Streaming Data" at ETH Zurich.
It contains a parallel implementation of an online regression method that is based on *regression element trees (RET)*.

## Installation
In order to use the "ret" package, you should follow these steps:
* clone (or fork) the repository
* create and activate a virtual environment
* navigate to the root folder of this project
* call `pip install -e .`

## Usage
The following code shows a small script that uses the regression element tree.

```Python
from mpi4py import MPI

from river.datasets.synth import Friedman
from river.metrics import MAE

from ret.regression_element_tree import RegressionElementTree
from ret.worker import do_work
from ret.message_passing import Communica`or

comm = Communicator(MPI.COMM_WORLD)
if comm.is_manager:
    dataset = Friedman()
    model = RegressionElementTree(comm, dataset.n_features)
    error = MAE()
    for x, y in dataset.take(1000):
        prediction = model.predict_one(x)
        error.update(y, prediction)
        model.learn_one(x, y)
    model.end_computation()
    print(error)
else:
    do_work(comm)
```
It should be run using the command `mpirun -n <number of processes> python <name of script>`

## Acknowledgements
The thesis is supervised by Dr. Daniel W. Meyer, who created *RET*.
He also contributed many design ideas to this project.

The project uses the online machine learning library [River](https://github.com/online-ml/river),
and parts of the code were inspired by its design.
